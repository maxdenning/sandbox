# Sandbox
Collection of small projects with shared code in `src/` and `include/`.


## Projects
* cpp/examples/hashmap

    Performance testing different hashmap implementations, including cache-aware and SIMD.
    Profiled with [Valgrind](valgrind.org).

* cpp/examples/vector

    Performance testing floating point vector dot product implementations, naive vs SIMD.

* cpp/examples/matmul

    GEMM implementation adapted from the [LAFF - On Programming for High Performance](ulaff.net) course.
    Uses [OMP](openmp.org) for parallelism and the [Eigen](eigen.tuxfamily.org) linear algebra package for benchmarking.
