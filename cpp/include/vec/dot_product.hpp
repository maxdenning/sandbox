#ifndef SANDBOX_DOTPRODUCT_H
#define SANDBOX_DOTPRODUCT_H

#include "simd_wrapper.hpp"

namespace sandbox
{


float dot_product_scalar(const float* a, const float* b, size_t size);

//float dot_product_simd_avx256(const float* a, const float* b, size_t size);

template<typename T, size_t N = 8>
float dot_product_simd(const T* a, const T* b, size_t size)
{
    using W = simd_wrapper<T, N>;  // simd wrapper type
 
    // check parameters
    if (size == 0)
    {
        return 0.0f;
    }

    if ((size % (4 * W::count)) != 0)
    {
        throw std::invalid_argument("size must be divisible by 32");
    }

    // initialise 4xN dot product accumulators
    typename W::vector_type acc0 = W::set_zero();
    typename W::vector_type acc1 = W::set_zero();
    typename W::vector_type acc2 = W::set_zero();
    typename W::vector_type acc3 = W::set_zero();

    // process groups of 4xN values to reduce dependency between instructions
    // use vertical FMA
    const typename W::value_type* const a_end = a + size;
    for (; a < a_end; (a += (W::count * 4), b += (W::count * 4)))
    {
        acc0 = W::fma(a,                  b,                  acc0);
        acc1 = W::fma(a +  W::count,      b +  W::count,      acc1);
        acc2 = W::fma(a + (W::count * 2), b + (W::count * 2), acc2);
        acc3 = W::fma(a + (W::count * 3), b + (W::count * 3), acc3);
    }

    // add 4xN value accumulators into 1xN values
    const typename W::vector_type acc01 = W::add(acc0, acc1);
    const typename W::vector_type acc23 = W::add(acc2, acc3);
    const typename W::vector_type acc = W::add(acc01, acc23);

    // horizontal add 1xN values
    return W::horz_add(acc);
}


}

#endif
