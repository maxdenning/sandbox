#ifndef _SANDBOX_ALIGNED_OASIMDHASHMAP_H_
#define _SANDBOX_ALIGNED_OASIMDHASHMAP_H_

#include <cstdint>
#include <memory>
#include <immintrin.h>
#include <type_traits>
#include <limits>
#include "hashmap.hpp"
#include "simd_wrapper.hpp"

namespace sandbox
{


template<typename K, typename M>
class AlignedOASIMDHashMap
{
    public:
        using key_type = typename std::enable_if_t<std::is_integral_v<K>>;
        using mapped_type = M;

        AlignedOASIMDHashMap(size_t table_size);
        ~AlignedOASIMDHashMap();

        void insert(HashMapEntry<K, M>&& value);
        M erase(const K key);
        M& at(const K key);

        size_t bucket_count() const;
        uint32_t collision_count() const;
        size_t bucket_depth(size_t index) const;

    private:
        using W = simd_wrapper<K, 32 / sizeof(K)>;
        using vector_type = typename W::vector_type;

        static constexpr K IS_AVAILABLE = std::numeric_limits<K>::max();
        static constexpr uint8_t AVX2_ALIGN = 32;

        struct alignas(AVX2_ALIGN) KeyBlock
        {
            K keys[W::count];
        };

        const std::hash<K> hasher;
        const size_t m_table_size;
        KeyBlock* const m_key_blocks;
        std::unique_ptr<M>* const m_values;
};


}

#endif
