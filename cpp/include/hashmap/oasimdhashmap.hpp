#ifndef _SANDBOX_OASIMDHASHMAP_H_
#define _SANDBOX_OASIMDHASHMAP_H_

#include <cstdint>
#include <memory>
#include <immintrin.h>
#include <limits>
#include "hashmap.hpp"
#include "simd_wrapper.hpp"

namespace sandbox
{


template<typename K, typename M>
class OASIMDHashMap
{
    public:
        using key_type = typename std::enable_if_t<std::is_integral_v<K>>;
        using mapped_type = M;

        OASIMDHashMap(size_t table_size);
        ~OASIMDHashMap();

        void insert(HashMapEntry<K, M>&& value);
        M erase(const K key);
        M& at(const K key);

        size_t bucket_count() const;
        uint32_t collision_count() const;
        size_t bucket_depth(size_t index) const;

    private:
        using W = simd_wrapper<K, 32 / sizeof(K)>;
        using vector_type = typename W::vector_type;

        static constexpr K IS_AVAILABLE = std::numeric_limits<K>::max();

        size_t find_first(const size_t start, const K value) const;

        const std::hash<K> hasher;
        const size_t m_max_table_index;
        const size_t m_table_size;  // larger than max index to allow for read overhang
        K* const m_keys;
        std::unique_ptr<M>* const m_values;
};


}

#endif
