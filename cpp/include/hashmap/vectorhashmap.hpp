#ifndef _SANDBOX_VECTORHASHMAP_H_
#define _SANDBOX_VECTORHASHMAP_H_

#include <cstdint>
#include <vector>
#include "hashmap.hpp"

namespace sandbox
{


template<typename K, typename M>
class VectorHashMap : public HashMap<K, M>
{
    public:
        VectorHashMap(size_t table_size);
        ~VectorHashMap();

        void insert(HashMapEntry<K, M>&& entry);
        M erase(const K& key);
        M& at(const K& key);
        size_t bucket_count() const;

    private:
        size_t bucket_depth(size_t index) const;

        const size_t m_table_size;
        std::vector<HashMapEntry<K, M>>* m_table;
};


}

#endif
