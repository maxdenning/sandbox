#ifndef _SANDBOX_OPENADDRESSINGHASHMAP_H_
#define _SANDBOX_OPENADDRESSINGHASHMAP_H_

#include <cstdint>
#include <array>
#include <memory>
#include <bitset>
#include "hashmap.hpp"

namespace sandbox
{


template<typename K, typename M>
class OpenAddressingHashMap : public HashMap<K, M>
{
    public:
        OpenAddressingHashMap(size_t table_size);
        ~OpenAddressingHashMap();

        void insert(HashMapEntry<K, M>&& value);
        M erase(const K& key);
        M& at(const K& key);
        size_t bucket_count() const;

    private:
        //static constexpr size_t CACHE_LINE = 64U;
        //static constexpr uint8_t N_PREFETCHED_KEYS = (CACHE_LINE - 30) / (sizeof(K) + sizeof(bool));

        /*struct KeyNode
        {
            KeyNode() = default;
            K key[N_PREFETCHED_KEYS];
            //bool occupied[N_PREFETCHED_KEYS];

            std::bitset<N_PREFETCHED_KEYS> occu;

            // TODO:
            //      make occupied a bitset/similar which encodes both presence and used depth of container
            //      e.g. 0000000 = [f, f, f, f], depth=0
            //           0000001 = [t, f, f, f], depth=1, (== (2 - 1))
            //           0000011 = [t, t, f, f], depth=2, (== (4 - 1))
            //           0000111 = [t, t, t, f], depth=3, (== (8 - 1))
            //           0000101 = [t, f, t, f], depth=3, (== (8 - 1))
        };*/


        struct KeyNode
        {
            KeyNode();
            K key;
            bool occupied;
        };

        /*struct ValueNode
        {
            M value[N_PREFETCHED_KEYS];
        };*/




        size_t bucket_depth(size_t index) const;

        const size_t m_table_size;
        //HashMapEntry<K, M>* m_table;
        
        //std::unique_ptr<KeyNode>* m_keys;
        //std::unique_ptr<ValueNode>* m_values;
        //bool* m_table_occupied;

        KeyNode* m_keys;
        M* m_values;


};


}

#endif
