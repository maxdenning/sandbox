#ifndef _SANDBOX_HASHMAP_H_
#define _SANDBOX_HASHMAP_H_

#include <cstdint>
#include <utility>
#include <vector>
#include <memory>


namespace sandbox
{


template<typename K, typename M>
struct HashMapEntry
{
    K key;
    M value;
};

template<typename K, typename M>
class HashMap
{
    public:
        using key_type = K;
        using mapped_type = M;

        HashMap();

        virtual size_t bucket_count() const = 0;
        uint32_t report_collisions() const;
        std::vector<size_t> report_bucket_depths() const;

    protected:
        size_t bucket_index(const K& key, size_t bucket_count);
        virtual size_t bucket_depth(size_t index) const = 0;

        uint64_t m_collisions;
        std::hash<K> hasher;
};


}

#endif
