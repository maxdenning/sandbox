#ifndef _SANDBOX_LINKEDLISTHASHMAP_H_
#define _SANDBOX_LINKEDLISTHASHMAP_H_

#include <cstdint>
#include <utility>
#include <vector>
#include <memory>
#include "hashmap.hpp"

namespace sandbox {


template<typename K, typename M>
class LinkedListHashMap : public HashMap<K, M>
{
    public:
        LinkedListHashMap(size_t table_size);
        ~LinkedListHashMap();

        void insert(HashMapEntry<K, M>&& entry);
        M erase(const K& key);
        M& at(const K& key);
        size_t bucket_count() const;

    private:
        struct Node
        {
            Node(HashMapEntry<K, M>&& entry);
            size_t size() const;

            HashMapEntry<K, M> entry;
            std::unique_ptr<Node> next;
        };

        size_t bucket_depth(size_t index) const;

        const size_t m_table_size;
        std::unique_ptr<Node>* m_table;
};


}

#endif
