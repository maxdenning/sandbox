#ifndef _SANDBOX_CACHEAWAREHASHMAP_H_
#define _SANDBOX_CACHEAWAREHASHMAP_H_

#include <cstdint>
#include <array>
#include <memory>
#include "hashmap.hpp"

// round to multiple which is a power of 2
#define ROUND2(num, multiple) (((num) + (multiple) - 1) & -(multiple))

namespace sandbox
{


template<typename K, typename M>
class CacheAwareHashMap : public HashMap<K, M>
{
    public:
        CacheAwareHashMap(size_t table_size);
        ~CacheAwareHashMap();

        void insert(HashMapEntry<K, M>&& value);
        M erase(const K& key);
        M& at(const K& key);
        size_t bucket_count() const;

    private:
        struct Node
        {
            //static constexpr size_t NODE_SIZE = sizeof(typename std::array<HashMap<K, M>, 0>::iterator) + sizeof(std::unique_ptr<Node>);
            //static constexpr size_t N_PREFETCHED_NODES = (64U - NODE_SIZE) / sizeof(HashMapEntry<K, M>);

            static constexpr size_t CACHE_LINE = 64U;
            static constexpr uint8_t NODE_SIZE = ROUND2(sizeof(uint8_t), 8) + ROUND2(sizeof(std::unique_ptr<Node>), 8);
            static constexpr uint8_t N_PREFETCHED_NODES = (CACHE_LINE - NODE_SIZE) / (sizeof(K) + sizeof(M));

            Node();
            Node(HashMapEntry<K, M>&& entry);
            ~Node();

            inline void push_back(HashMapEntry<K, M>&& entry);
            //void insert(HashMapEntry<K, M>&& entry);
            inline bool is_full() const;
            size_t size() const;

            //std::array<HashMapEntry<K, M>, N_PREFETCHED_NODES> entries;
            //typename std::array<HashMapEntry<K, M>, N_PREFETCHED_NODES>::iterator back;

            //std::array<K, N_PREFETCHED_NODES> keys;
            //std::array<M, N_PREFETCHED_NODES> values;
            //std::unique_ptr<Node> next;
            Node* next;
            uint8_t back;
            K keys[N_PREFETCHED_NODES];
            M values[N_PREFETCHED_NODES];
        };

        size_t bucket_depth(size_t index) const;

        const size_t m_table_size;
        std::unique_ptr<Node>* m_table;

    //public:
        //static constexpr size_t N_PREFETCHED_NODES = (64U - sizeof(CacheAwareHashMap<K, M>)) / sizeof(Node);
        //using bucket_t = std::array<Node, N_PREFETCHED_NODES>;
        //std::unique_ptr<bucket_t>* m_table;
};


}

#undef ROUND2
#endif
