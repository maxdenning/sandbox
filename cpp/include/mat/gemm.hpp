#ifndef SANDBOX_GEMM_H
#define SANDBOX_GEMM_H

#include "matrix.hpp"

namespace sandbox
{


//
// GEneral Matrix Multiplication, C := AB + C
//

void gemm_ijp(const MatrixD& a, const MatrixD& b, MatrixD& c);
void gemm_ipj(const MatrixD& a, const MatrixD& b, MatrixD& c);
void gemm_jip(const MatrixD& a, const MatrixD& b, MatrixD& c);
void gemm_jpi(const MatrixD& a, const MatrixD& b, MatrixD& c);
void gemm_pij(const MatrixD& a, const MatrixD& b, MatrixD& c);
void gemm_pji(const MatrixD& a, const MatrixD& b, MatrixD& c);

void gemm_gemv_jpi(const MatrixD& a, const MatrixD& b, MatrixD& c);
void gemm_ger_pji(const MatrixD& a, const MatrixD& b, MatrixD& c);
void gemm_ger_pij(const MatrixD& a, const MatrixD& b, MatrixD& c);

void gemm_block_ger_ji(const MatrixD& a, const MatrixD& b, MatrixD& c, size_t mb = 4, size_t nb = 4);
void gemm_block_ger_ij(const MatrixD& a, const MatrixD& b, MatrixD& c, size_t mb = 4, size_t nb = 4);

// mb must be 4, 8, or 12
void gemm_block_ji_mbx4(const MatrixD& a, const MatrixD& b, MatrixD& c, size_t mb = 4);
void gemm_cached_pi_ji_mbx4(const MatrixD& a, const MatrixD& b, MatrixD& c, size_t mb = 4);

void gemm_cached_jpi_ji_12x4(const MatrixD& a, const MatrixD& b, MatrixD& c);
void gemm_packed_jpi_ji_12x4(const MatrixD& a, const MatrixD& b, MatrixD& c);

void gemm_mp_ijp(const MatrixD& a, const MatrixD& b, MatrixD& c);
void gemm_mp_jpi(const MatrixD& a, const MatrixD& b, MatrixD& c);
void gemm_mp_packed_jpi_ji_12x4(const MatrixD& a, const MatrixD& b, MatrixD& c);


//
// Helpers
//

/*
    Returns true if matrix dimensions are compatible for a C := AB + C operation
*/
bool are_dimensions_compatible(const MatrixD& a, const MatrixD& b, const MatrixD& c);

/*
*/
using FnKernel = void(*)(size_t, const double* const, size_t, const double* const, size_t, double* const, size_t);
FnKernel get_kernel(size_t mb);

/*
    Y := aX + Y
    where
        a is scalar,
        X is a row/column vector of (m x 1)/(1 x m),
        Y is a row/column vector of (m x 1)/(1 x m),

    set x_ld if x is a row vector
    set y_ld if y is a row vector
*/
void axpy(double alpha, size_t m, const double* const __restrict__ x, double* const __restrict__ y, size_t x_ld = 1, size_t y_ld = 1);

/*
    y := Ax + y
    where
        A is (m x k),
        x is column vector of (k x 1),
        y is a column vector of (m x 1),
*/
void gemv(const MatrixD& a, const double* const __restrict__ x, double* const __restrict__ y);

/*
    A := xy^t + A
    where
        x is a column vector of (m x 1)
        y is a row vector of (1 x n)
        A is (m x n)
    
    y_ld is the stride (leading dimension) between columns in y
*/
void ger_ji(MatrixD& a, const double* const __restrict__ x, const double* const __restrict__ y, size_t y_ld);
void ger_ij(MatrixD& a, const double* const __restrict__ x, const double* const __restrict__ y, size_t y_ld);

/*
    C := AB + C
    where
        C is (4 x 4)
        A is (4 x k)
        B is (k x 4)
*/
void kernel_4x4(size_t k, const double* const __restrict__ a, size_t a_ld, const double* const __restrict__ b, size_t b_ld, double* const __restrict__ c, size_t c_ld);

/*
    C := AB + C
    where
        C is (8 x 4)
        A is (8 x k)
        B is (k x 4)
*/
void kernel_8x4(size_t k, const double* const __restrict__ a, size_t a_ld, const double* const __restrict__ b, size_t b_ld, double* const __restrict__ c, size_t c_ld);

/*
    C := AB + C
    where
        C is (12 x 4)
        A is (12 x k)
        B is (k x 4)
*/
void kernel_12x4(size_t k, const double* const __restrict__ a, size_t a_ld, const double* const __restrict__ b, size_t b_ld, double* const __restrict__ c, size_t c_ld);

/*
    C := AB + C
    where
        C is (m x n)
        A is (m x k)
        B is (k x n)
    
    m must be a multiple of 4
    n must be a multiple of 4
*/
void kernel_ji_mbx4(FnKernel fn_kernel, size_t mb, size_t m, size_t n, size_t k, const double* const __restrict__ a, size_t a_ld, const double* const __restrict__ b, size_t b_ld, double* const __restrict__ c, size_t c_ld);

/*
    C := AB + C
    where
        C is (mb x 4)
        A is a packed buffer of contiguous column values (mb x k)
        B is a packed buffer of contiguous row values (k x 4)
*/
void kernel_packed_12x4(size_t k, const double* const __restrict__ a, const double* const __restrict__ b, double* const __restrict__ c, size_t c_ld);

/*
    Packs a column-major row-panel src (m x n) into contiguous column micro-panels dst (m x n)
    Each column micro panel is (m x nb)
*/
void pack_row_panel(size_t m, size_t n, size_t nb, const double* const __restrict__ src, size_t src_ld, double* const __restrict__ dst);

/*
    Packs a column-major column micro-panel (m x n) into a contiguous column micro-panel (m x n)
*/
void pack_column_micropanel(size_t m, size_t n, const double* const __restrict__ src, size_t src_ld, double* __restrict__ dst);

/*
    Packs a column-major tile src (m x n) into contiguous row micro-panels dst (m x n)
    Each row micro-panel is (mb x n)
*/
void pack_tile(size_t m, size_t n, size_t mb, const double* const __restrict__ src, size_t src_ld, double* const __restrict__ dst);

/*
    Packs a column-major row micro-panel src (m x n) into a contiguous row micro-panel dst (m x n)
*/
void pack_row_micropanel(size_t m, size_t n, const double* const __restrict__ src, size_t src_ld, double* __restrict__ dst);


}

#endif
