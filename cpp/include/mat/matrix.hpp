#ifndef SANDBOX_MATRIX_H
#define SANDBOX_MATRIX_H

#include <ostream>
#include <tuple>

#define RAVEL_INDEX(i, j, ld) (((j) * (ld)) + (i))

namespace sandbox
{


template<typename T>
class Matrix
{
    public:
        Matrix(size_t rows, size_t cols, T* const data, size_t ld = 0);
        Matrix(const Matrix& m) = default;
        Matrix(Matrix&& m) = default;

        //
        static std::tuple<T*, Matrix<T>> zeroes(size_t rows, size_t cols);
        static std::tuple<T*, Matrix<T>> full(size_t rows, size_t cols, T value);
        Matrix<T> view(size_t rows, size_t cols, size_t start_i, size_t start_j) const;

        //
        inline size_t cols() const;
        inline size_t rows() const;
        inline size_t ld() const;
        inline T* data() const;

        //
        inline T& operator()(size_t i, size_t j) const;  //TODO: this isn't really const
        inline T& operator()(size_t i, size_t j);
        inline T operator[](size_t i) const;
        inline T& operator[](size_t i);

    private:
        const size_t m_m;  // number of rows (i)
        const size_t m_n;  // number of columns (j)
        const size_t m_ld;  // leading dimension (m) of parent matrix, used to traverse columns
        T* const m_data;  // flat array of values organised by column, may be part of a larger matrix
};


//////////////////////////////////////////////////////////////////////////////////////////////////
// UTILS
//////////////////////////////////////////////////////////////////////////////////////////////////

static inline size_t ravel_index(size_t i, size_t j, size_t ld)
{
    return (j * ld) + i;
}

//std::tuple<size_t, size_t> unravel_index(size_t idx, size_t ld);

template<typename T>
std::ostream& operator<<(std::ostream& os, const Matrix<T>& m)
{
    os << "[";
    for (size_t i = 0; i < m.rows(); i++)
    {
        os << "[";
        for (size_t j = 0; j < m.cols(); j++)
        {
            os << " " << m(i, j);
        }
        os << " ]";

        if (i < m.rows() - 1)
        {
            os << ", " << std::endl;
        }
    }
    os << "]";
    return os;
}


//
using MatrixD = Matrix<double>;
using MatrixF = Matrix<float>;

}

#include "matrix.tpp"  // include template implementation

#endif
