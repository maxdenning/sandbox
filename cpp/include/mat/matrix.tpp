#ifndef SANDBOX_MATRIX_T
#define SANDBOX_MATRIX_T

#include <cstdlib>
#include <algorithm>
#include "matrix.hpp"

#include <iostream>

namespace sandbox
{

template<typename T>
Matrix<T>::Matrix(size_t rows, size_t cols, T* const data, size_t ld)
    : m_m(rows),
      m_n(cols),
      m_ld(ld <= 0 ? rows : ld),
      m_data(data)
{
}

template<typename T>
std::tuple<T*, Matrix<T>> Matrix<T>::zeroes(size_t rows, size_t cols)
{
    T* const data = (T*)std::calloc(rows * cols, sizeof(T));
    return { data, Matrix<T>(rows, cols, data) };
}

template<typename T>
std::tuple<T*, Matrix<T>> Matrix<T>::full(size_t rows, size_t cols, T value)
{
    T* const data = new T[rows * cols];
    std::fill_n(data, rows * cols, value);
    return { data, Matrix<T>(rows, cols, data) };
}

template<typename T>
Matrix<T> Matrix<T>::view(size_t rows, size_t cols, size_t start_i, size_t start_j) const
{
    // if (start_i + rows > m_m || start_j + cols > m_n)
    // {
    //     throw std::invalid_argument("Requested submatrix is out of bounds");
    // }
    return Matrix(rows, cols, &m_data[ravel_index(start_i, start_j, m_ld)], m_ld);
}

template<typename T>
size_t Matrix<T>::rows() const
{
    return m_m;
}

template<typename T>
size_t Matrix<T>::cols() const
{
    return m_n;
}

template<typename T>
size_t Matrix<T>::ld() const
{
    return m_ld;
}

template<typename T>
T* Matrix<T>::data() const
{
    return m_data;
}

template<typename T>
T& Matrix<T>::operator()(size_t i, size_t j) const
{
    // if (i >= m_m || j >= m_n)
    // {
    //     throw std::invalid_argument("Index out of bounds");
    // }
    return m_data[RAVEL_INDEX(i, j, m_ld)];  //ravel_index(i, j, m_ld)];
}

template<typename T>
T& Matrix<T>::operator()(size_t i, size_t j)
{
    // if (i >= m_m || j >= m_n)
    // {
    //     throw std::invalid_argument("Index out of bounds");
    // }
    return m_data[RAVEL_INDEX(i, j, m_ld)];  //ravel_index(i, j, m_ld)];
}

template<typename T>
T Matrix<T>::operator[](size_t i) const
{
    // if (i >= m_m || j >= m_n)
    // {
    //     throw std::invalid_argument("Index out of bounds");
    // }
    return m_data[i];
}

template<typename T>
T& Matrix<T>::operator[](size_t i)
{
    // if (i >= m_m || j >= m_n)
    // {
    //     throw std::invalid_argument("Index out of bounds");
    // }
    return m_data[i];
}


}

#endif
