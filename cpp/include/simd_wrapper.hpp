#ifndef SANDBOX_SIMDWRAPPER_H
#define SANDBOX_SIMDWRAPPER_H

#include <immintrin.h>
#include <cstdint>
#include <ostream>
#include <array>

namespace sandbox
{


std::ostream& operator<<(std::ostream& os, __m256 v);
std::ostream& operator<<(std::ostream& os, __m128 v);

template<typename T>
std::array<T, (32U / sizeof(T))> to_array(__m256i v)
{
    std::array<T, (32U / sizeof(T))> view;
    _mm256_storeu_si256((__m256i*)view.data(), v);
    return view;
}

template<typename T, size_t N>
struct simd_wrapper
{
};

template<>
struct simd_wrapper<float, 4UL>
{
    using value_type = float;
    using vector_type = __m128;
    constexpr static size_t count = 4UL;

    static inline __m128 set_zero() { return _mm_setzero_ps(); }
    static inline __m128 full(float v) { return _mm_set1_ps(v); }
    static inline __m128 load(const float* const a) { return _mm_load_ps(a); }

    static inline __m128 div(__m128 a, __m128 b) { return _mm_div_ps(a, b); }
    static inline __m128 div(const float* const a, const float* const b) { return div(load(a), load(b)); }

    static inline __m128 mul(__m128 a, __m128 b) { return _mm_mul_ps(a, b); }
    static inline __m128 mul(const float* const a, const float* const b) { return mul(load(a), load(b)); }

    static inline __m128 fma(__m128 a, __m128 b, __m128 acc) { return _mm_fmadd_ps(a, b, acc); }
    static inline __m128 fma(const float* const a, const float* const b, __m128 acc) { return fma(load(a), load(b), acc); }

    static inline __m128 add(__m128 a, __m128 b) { return _mm_add_ps(a, b); }
    static inline float horz_add(__m128 a)
    {
        // add upper and lower halves of 1x4 floats into 1x2 floats
        const __m128 upper2 = _mm_movehl_ps(a, a);
        const __m128 reduce2 = _mm_add_ps(a, upper2);

        // add 2 values into 1
        const __m128 dup = _mm_movehdup_ps(reduce2);
        const __m128 reduce1 = _mm_add_ss(reduce2, dup);

        // return lowest lane of result
        return _mm_cvtss_f32(reduce1);
    }
};

template<>
struct simd_wrapper<float, 8UL>
{
    using value_type = float;
    using vector_type = __m256;
    constexpr static size_t count = 8UL;

    static inline __m256 set_zero() { return _mm256_setzero_ps(); }
    static inline __m256 full(float v) { return _mm256_set1_ps(v); }
    static inline __m256 load(const float* const a) { return _mm256_load_ps(a); }

    static inline __m256 div(__m256 a, __m256 b) { return _mm256_div_ps(a, b); }
    static inline __m256 div(const float* const a, const float* const b) { return div(load(a), load(b)); }

    static inline __m256 mul(__m256 a, __m256 b) { return _mm256_mul_ps(a, b); }
    static inline __m256 mul(const float* const a, const float* const b) { return mul(load(a), load(b)); }

    static inline __m256 fma(__m256 a, __m256 b, __m256 acc) { return _mm256_fmadd_ps(a, b, acc); }
    static inline __m256 fma(const float* const a, const float* const b, __m256 acc) { return fma(load(a), load(b), acc); }

    static inline __m256 add(__m256 a, __m256 b) { return _mm256_add_ps(a, b); }
    static inline float horz_add(__m256 a)
    {
        // add upper and lower halves of 1x8 floats into 1x4 floats
        const __m128 upper4 = _mm256_castps256_ps128(a);
        const __m128 lower4 = _mm256_extractf128_ps(a, 1);
        const __m128 reduce4 = _mm_add_ps(upper4, lower4);

        // horizontal add 1x4 floats
        return simd_wrapper<float, 4>::horz_add(reduce4);
    }
};

template<>
struct simd_wrapper<uint64_t, 4UL>
{
    using value_type = uint64_t;
    using vector_type = __m256i;
    constexpr static size_t count = 4UL;

    static inline __m256i set_zero() { return _mm256_setzero_si256(); }
    static inline __m256i full(uint64_t v) { return _mm256_set1_epi64x(v); }
    static inline __m256i load(const uint64_t* const a) { return _mm256_load_si256((__m256i*)a); }
    static inline __m256i loadu(const uint64_t* const a) { return _mm256_loadu_si256((__m256i*)a); }

    static inline __m256i cmp_eq(__m256i a, __m256i b) { return _mm256_cmpeq_epi64(a, b); }
};

template<>
struct simd_wrapper<uint32_t, 8UL>
{
    using value_type = uint32_t;
    using vector_type = __m256i;
    constexpr static size_t count = 8UL;

    static inline __m256i set_zero() { return _mm256_setzero_si256(); }
    static inline __m256i full(uint32_t v) { return _mm256_set1_epi32(v); }
    static inline __m256i load(const uint32_t* const a) { return _mm256_load_si256((__m256i*)a); }
    static inline __m256i loadu(const uint32_t* const a) { return _mm256_loadu_si256((__m256i*)a); }

    static inline __m256i cmp_eq(__m256i a, __m256i b) { return _mm256_cmpeq_epi32(a, b); }
};

template<>
struct simd_wrapper<uint16_t, 16UL>
{
    using value_type = uint16_t;
    using vector_type = __m256i;
    constexpr static size_t count = 16UL;

    static inline __m256i set_zero() { return _mm256_setzero_si256(); }
    static inline __m256i full(uint16_t v) { return _mm256_set1_epi16(v); }
    static inline __m256i load(const uint16_t* const a) { return _mm256_load_si256((__m256i*)a); }
    static inline __m256i loadu(const uint16_t* const a) { return _mm256_loadu_si256((__m256i*)a); }

    static inline __m256i cmp_eq(__m256i a, __m256i b) { return _mm256_cmpeq_epi16(a, b); }
};



}

#endif
