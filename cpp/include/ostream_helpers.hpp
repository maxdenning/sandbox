#ifndef SANDBOX_OSTREAM_HELPERS_H
#define SANDBOX_OSTREAM_HELPERS_H

#include <ostream>
#include <vector>
#include <map>
#include <array>

namespace sandbox
{


template<typename T, size_t N>
std::ostream& operator<<(std::ostream& os, const std::array<T, N>& v)
{
    os << "[";
    for (size_t i = 0; i < N; i++)
    {
        os << " " << v[i];
    }
    os << " ]";
    return os;
} 

template<typename T>
static std::ostream& operator<<(std::ostream& os, const std::vector<T>& v)
{
    os << "[";
    for (auto itr = v.begin(); itr != v.end(); itr++)
    {
        os << " " << *itr;
    }
    os << " ]";
    return os;
}

template<typename K, typename M>
static std::ostream& operator<<(std::ostream& os, const std::map<K, M>& v)
{
    os << "[";
    for (auto itr = v.begin(); itr != v.end(); itr++)
    {
        os << " " << *itr; //" {" << itr->first << ": " << itr->second << "}";
    }
    os << " ]";
    return os;
}

template<typename A, typename B>
static std::ostream& operator<<(std::ostream& os, const std::pair<A, B>& v)
{
    os << "{" << v.first << ": " << v.second << "}";
    return os;
}

/*
    Print raw C-style array using a tuple of pointer and size
*/
template<typename T>
static std::ostream& operator<<(std::ostream& os, const std::tuple<T*, size_t>& v)
{
    const T* const arr = std::get<0>(v);
    const size_t sz = std::get<1>(v);

    os << "[";
    for (size_t i = 0; i < sz; i++)
    {
        os << " " << arr[i];
    }
    os << " ]" << std::endl;
    return os;
}

}

#endif
