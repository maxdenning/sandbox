# import numpy as np
import matplotlib.pyplot as plt
import sys
import os


def get_csv_filepaths(mixed_paths):
    filepaths = []
    for path in mixed_paths:
        if os.path.isfile(path):
            if path[-4:] == ".csv":
                filepaths.append(path)

        elif os.path.isdir(path):
            for file in os.listdir(path):
                if file.endswith(".csv"):
                    filepaths.append(os.path.join(path, file))

    return filepaths


def read_csv(filepath):
    matrix_size = []  # square matrices, i.e. m = n = k
    mean_duration = []  # seconds

    with open(filepath, "r") as ifile:
        for line in ifile.readlines():
            row = list(map(int, line.strip().split(",")))

            if row[0] != row[1] != row[2]:
                raise ValueError("Must be a square matrix, m != n != k")

            matrix_size.append(row[0])
            mean_duration.append((sum(row[4:]) / row[3]) / 1.0e6)  # average and convert from microseconds to seconds

    return matrix_size, mean_duration


def duration_to_gflops(m, n, k, duration_seconds):
    # C := AB + C requires 2mnk flops (where C is mxn, A is mxk and B is kxn)
    total_flop = 2 * m * n * k
    return (total_flop / duration_seconds) / 1.0e9


if __name__ == "__main__":
    #
    if len(sys.argv) < 2:
        print(f"help: {sys.argv[0]} <csv paths...>")
        exit(1)

    filepaths = get_csv_filepaths(sys.argv[1:])

    #
    CORE_SPEED = 2.9  # GHz
    CORES = 4
    INSTRUCTIONS_PER_CYCLE = 16  # haswell, 16 double precision ops per cycle
    theoretical_max_gflops = CORE_SPEED * CORES * INSTRUCTIONS_PER_CYCLE

    #
    cmap = plt.cm.get_cmap("hsv", len(filepaths) + 1)
    plt.xlabel("matrix size")
    plt.ylabel("gflops")
    # plt.yticks(range(0, int(theoretical_max_gflops) + 5, 5))
    # plt.ylim(bottom=0, top=theoretical_max_gflops + 5)
    plt.axhline(y=theoretical_max_gflops, label="max", color=cmap(len(filepaths)))

    #
    for i, filepath in enumerate(filepaths):
        sizes, durations = read_csv(filepath)
        gflops = [duration_to_gflops(s, s, s, d) for (s, d) in zip(sizes, durations)]

        name = os.path.splitext(os.path.basename(filepath))[0]
        plt.plot(sizes, gflops, label=name, color=cmap(i))

        print(name, list(zip(sizes, gflops)))

    plt.legend()
    plt.show()

    exit(0)
