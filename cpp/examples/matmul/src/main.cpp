#include <algorithm>
#include <chrono>
#include <functional>
#include <getopt.h>
#include <iostream>
#include <iterator>
#include <random>
#include <sstream>
#include <vector>

#include "mat/matrix.hpp"
#include "mat/gemm.hpp"
#include "ostream_helpers.hpp"

#include <Eigen/Dense>

using namespace sandbox;


void benchmark_eigen(const MatrixD& a, const MatrixD& b, MatrixD& c)
{
    Eigen::Map<Eigen::MatrixXd> eigen_a(a.data(), a.rows(), a.cols());
    Eigen::Map<Eigen::MatrixXd> eigen_b(b.data(), b.rows(), b.cols());
    Eigen::Map<Eigen::MatrixXd> eigen_c(c.data(), c.rows(), c.cols());
    eigen_c += eigen_a * eigen_b;
}

using FnMatmul = std::function<void/*(*)*/(const MatrixD&, const MatrixD&, MatrixD&)>;

FnMatmul get_matmul_strategy(const std::string& name)
{
    FnMatmul strategy = nullptr;

    // benchmark
    if (name == "eigen") strategy = benchmark_eigen;

    // simple approach
    else if (name == "ijp") strategy = gemm_ijp;
    else if (name == "ipj") strategy = gemm_ipj;
    else if (name == "jip") strategy = gemm_jip;
    else if (name == "jpi") strategy = gemm_jpi;
    else if (name == "pij") strategy = gemm_pij;
    else if (name == "pji") strategy = gemm_pji;

    // vector approach
    else if (name == "gemv_jpi") strategy = gemm_gemv_jpi;
    else if (name == "ger_ji")   strategy = gemm_ger_pji;
    else if (name == "ger_ij")   strategy = gemm_ger_pij;

    // blocked approach
    else if (name == "block_ger_ji_8") strategy = [](auto a, auto b, auto c) { return gemm_block_ger_ji(a, b, c, 8, 8); };
    else if (name == "block_ger_ij_8") strategy = [](auto a, auto b, auto c) { return gemm_block_ger_ij(a, b, c, 8, 8); };
    else if (name == "block_4x4")      strategy = [](auto a, auto b, auto c) { return gemm_block_ji_mbx4(a, b, c, 4); };
    else if (name == "block_8x4")      strategy = [](auto a, auto b, auto c) { return gemm_block_ji_mbx4(a, b, c, 8); };
    else if (name == "block_12x4")     strategy = [](auto a, auto b, auto c) { return gemm_block_ji_mbx4(a, b, c, 12); };
    else if (name == "cached_4x4")     strategy = [](auto a, auto b, auto c) { return gemm_cached_pi_ji_mbx4(a, b, c, 4); };
    else if (name == "cached_8x4")     strategy = [](auto a, auto b, auto c) { return gemm_cached_pi_ji_mbx4(a, b, c, 8); };
    else if (name == "cached_12x4")    strategy = [](auto a, auto b, auto c) { return gemm_cached_pi_ji_mbx4(a, b, c, 12); };
    else if (name == "cached_5l_12x4") strategy = gemm_cached_jpi_ji_12x4;

    // packed approach
    else if (name == "packed_12x4") strategy = gemm_packed_jpi_ji_12x4;

    // OMP approach
    else if (name == "mp_ijp") strategy = gemm_mp_ijp;
    else if (name == "mp_jpi") strategy = gemm_mp_jpi;
    else if (name == "mp_packed_12x4") strategy = gemm_mp_packed_jpi_ji_12x4;

    // error
    else throw std::invalid_argument("Invalid strategy name");

    return strategy;
}

struct TestResults
{
    TestResults(uint8_t r, size_t m, size_t n, size_t k)
        : m(m), n(n), k(k), repeats(r), correct(false)
    {
        durations.reserve(repeats);
    }

    size_t m;
    size_t n;
    size_t k;
    uint8_t repeats;
    bool correct;
    std::vector<std::chrono::microseconds> durations;

    friend std::ostream& operator<<(std::ostream& os, const TestResults& r)
    {
        const std::string_view sep = ",";
        os << r.m << sep << r.n << sep << r.k << sep << (int)r.repeats;
        for (uint8_t i = 0; i < r.repeats; i++)
        {
            os << sep << r.durations[i].count();
        }
        return os;
    }
};

TestResults test(uint8_t repeats, size_t m, size_t n, size_t k, FnMatmul fn_matmul, uint8_t warmup = 0, bool compare = false, bool verbose = false)
{
    // allocate
    const size_t count_a = m * k;
    const size_t count_b = k * n;
    const size_t count_c = m * n;

    double* const raw_a  = new double[count_a];
    double* const raw_b  = new double[count_b];
    double* const raw_c1 = new double[count_c];
    double* const raw_c2 = new double[count_c];

    MatrixD mat_a = MatrixD(m, k, raw_a);
    MatrixD mat_b = MatrixD(k, n, raw_b);
    MatrixD mat_c = MatrixD(m, n, raw_c1);

    Eigen::Map<Eigen::MatrixXd> cmp_a(raw_a, m, k);
    Eigen::Map<Eigen::MatrixXd> cmp_b(raw_b, k, n);
    Eigen::Map<Eigen::MatrixXd> cmp_c1(raw_c1, m, n);
    Eigen::Map<Eigen::MatrixXd> cmp_c2(raw_c2, m, n);

    // rng
    std::mt19937 rng(0);
    std::uniform_real_distribution<double> dist(0.0, 100.0);
    const auto fn_gen_rand = [&](){ return dist(rng); };

    // tests
    TestResults results(repeats, m, n, k);
    for (uint8_t r = 0; r < repeats + warmup; r++)
    {
        // fill matrices, c1 and c2 are copies
        std::generate_n(raw_a,  count_a, std::ref(fn_gen_rand));
        std::generate_n(raw_b,  count_b, std::ref(fn_gen_rand));
        std::generate_n(raw_c1, count_c, std::ref(fn_gen_rand));
        std::copy_n(raw_c1, count_c, raw_c2);

        if (verbose)
        {
            std::cerr << "Problem:" << std::endl
                      << mat_a << std::endl << " *" << std::endl
                      << mat_b << std::endl << " +" << std::endl
                      << mat_c << std::endl;
        }

        // test & time
        const auto begin = std::chrono::steady_clock::now();
        fn_matmul(mat_a, mat_b, mat_c);
        const auto duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - begin);

        if (verbose)
        {
            std::cerr << "Attmpted solution:" << std::endl << mat_c << std::endl;
        }

        // confirm results using eigen
        if (compare)
        {
            cmp_c2 += cmp_a * cmp_b;

            if (verbose)
            {
                std::cerr << "Compared solution:" << std::endl << cmp_c2 << std::endl;
            }

            if (cmp_c2.isApprox(cmp_c1))
            {
                results.correct = true;
            }
            else
            {
                results.correct = false;
                break;
            }
        }

        // record results
        if (r >= warmup)
        {
            results.durations.push_back(duration);
        }
    }

    delete[] raw_a;
    delete[] raw_b;
    delete[] raw_c1;
    delete[] raw_c2;

    return results;
}

int main(int argc, char* argv[])
{
    // help message
    if (argc < 3)
    {
        std::cerr << "help: " << argv[0] << " <strategy> (opts...) <matrix dimensions list ...>" << std::endl
                  << "\t-w\twarmup iterations, default = 0" << std::endl
                  << "\t-r\trepeats, default = 1" << std::endl
                  << "\t-t\tcorrectness test flag (use dims list as MxNxK)" << std::endl;
        return 1;
    }

    // parse input args
    const FnMatmul strategy = get_matmul_strategy(argv[1]);
    uint8_t warmup = 0;
    uint8_t repeats = 1;
    bool is_correctness_test = false;
    bool is_verbose = false;
    std::vector<size_t> matrix_sizes;

    int c = 0;
    while((c = getopt(argc, argv, "w:r:cv")) != -1)
    {
        switch (c)
        {
            case 'w':
                warmup = (uint8_t)std::strtoull(optarg, nullptr, 10);
                break;
            
            case 'r':
                repeats = (uint8_t)std::strtoull(optarg, nullptr, 10);
                break;

            case 'c':
                is_correctness_test = true;
                break;

            case 'v':
                is_verbose = true;
                break;

            case '?':
            default:
                std::cerr << "Invalid option: " << (char)c << std::endl;
                return 1;
        }
    }

    // parse matrix dimensions
    const auto op = [](const std::string& s) -> size_t { return std::strtoull(s.c_str(), nullptr, 10); };
    std::transform(&argv[optind + 1], &argv[argc], std::back_inserter(matrix_sizes), std::ref(op));

    if (is_correctness_test)
    {
        // perform correctness test
        if (matrix_sizes.size() < 3)
        {
            std::cerr << "Insufficient matrix dimension arguments" << std::endl;
            return 1;
        }

        const TestResults results = test(repeats, matrix_sizes[0], matrix_sizes[1], matrix_sizes[2], strategy, warmup, true, is_verbose);
        if (!results.correct)
        {
            std::cerr << "Correctness test failed on repeat " << (int)results.durations.size() << " for matrix dims " << matrix_sizes << std::endl; //<< m << "x" << n << "x" << k << "" << std::endl;
            return 2;
        }
    }
    else
    {
        // perform  speed test
        for (size_t n : matrix_sizes)
        {
            const TestResults results = test(repeats, n, n, n, strategy, warmup, false, is_verbose);
            std::cout << results << std::endl;
        }
    }

    return 0;
}
