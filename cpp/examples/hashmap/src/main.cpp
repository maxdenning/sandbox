#include <cstddef>
#include <iostream>
#include <algorithm>
#include <vector>
#include <chrono>
#include <numeric>
#include <map>
#include <unordered_map>
#include <tuple>
#include <functional>
#include <random>
#include <type_traits>
#include <iomanip>
#include <string>
#include <unordered_set>
#include "type_name.hpp"
#include "ostream_helpers.hpp"
#include "hashmap/hashmap.hpp"
#include "hashmap/linkedlisthashmap.hpp"
#include "hashmap/vectorhashmap.hpp"
#include "hashmap/cacheawarehashmap.hpp"
#include "hashmap/openaddressinghashmap.hpp"
#include "hashmap/oasimdhashmap.hpp"
#include "hashmap/aligned_oasimd_hashmap.hpp"

using namespace sandbox;


enum MapType
{
    STD_UNORDERED, LINKED_LIST, VECTOR, CACHE_AWARE, OPEN_ADDR, OASIMD, AOS
};

MapType map_type_from_name(const std::string& name)
{
    if (name == "std")    return STD_UNORDERED;
    if (name == "list")   return LINKED_LIST;
    if (name == "vector") return VECTOR;
    if (name == "cache")  return CACHE_AWARE;
    if (name == "open")   return OPEN_ADDR;
    if (name == "oasimd") return OASIMD;
    if (name == "aos")    return AOS;
    throw std::invalid_argument("invalid map type name");
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

static uint64_t find_next_prime(uint64_t n)
{
    // does not check for even numbers or less than 2 as these are never given
    const auto is_prime = [](uint64_t candidate) {
        for (uint64_t i = 3; i * i <= candidate; i += 2)
        {
            if (candidate % i == 0)
            {
                return false; // found a factor that isn't 1 or n, therefore not prime
            }
        }
        return candidate > 1; // prime unless n = 1 (no divisors > 1 but not prime)
    };

    uint64_t next_prime = n + (((n & 1) && (n > 1) || !n) ? 2 : 1); // start at n + 2 for odd n > 1 or n = 0, else n + 1
    while (!is_prime(next_prime))
    {
        next_prime += 2;
    }
    return next_prime;
}

template<typename T, typename _ = void>
struct generate_data
{
};

template<>
struct generate_data<std::string>
{
    static std::vector<std::string> random(size_t size, uint64_t seed, size_t min_str_len = 8, size_t max_str_len = 16)
    {
        std::vector<std::string> data;
        data.reserve(size);

        std::mt19937_64 eng(seed);
        std::uniform_int_distribution<uint8_t> distribution;

        for (size_t i = 0; i < size; i++)
        {
            const size_t len = min_str_len + (distribution(eng) % (max_str_len - min_str_len));
            std::string value;
            value.reserve(len);
            for (size_t c = 0; c < len; c++)
            {
                value += (char)('a' + (distribution(eng) % ('z' - 'a')));
            }
            data.push_back(value);
        }
        return data;
    }
};

template<typename T>
struct generate_data<T, typename std::enable_if_t<std::is_integral_v<T>>>
{
    static std::vector<T> random(size_t size, uint64_t seed)
    {
        if (size >= (std::numeric_limits<T>::max() - std::numeric_limits<T>::min()))
        {
            throw std::invalid_argument("requested dataset size is greater than range of dataset type");
        }

        std::unordered_set<T> data;
        data.reserve(size);

        std::mt19937_64 eng(seed);
        std::uniform_int_distribution<T> distribution;

        /*for (size_t i = 0; i < size; i++)
        {
            const T value = distribution(eng);
            data.push_back(value);
        }*/

        // generate new values, avoiding duplicates
        size_t i = 0;
        while (i < size)
        {
            const T value = distribution(eng);
            if (value == std::numeric_limits<T>::max()) continue;  // reserve max value
            i += data.insert(value).second ? 1 : 0;
        }

        // copy to vector
        std::vector<T> data_copy;
        data_copy.reserve(size);
        std::copy(data.begin(), data.end(), std::back_inserter(data_copy));
        return data_copy;
    }
};

template<typename T>
static std::map<T, uint32_t> count(const std::vector<T>& values)
{
    std::map<T, uint32_t> counts;
    for (auto& v : values)
    {
        auto itr = counts.find(v);
        if (itr == counts.end())
        {
            counts[v] = 1;
        }
        else
        {
            itr->second += 1;
        }
    }
    return counts;
}

template<typename T>
static std::map<T, uint32_t> merge_sum(const std::vector<std::map<T, uint32_t>>& dicts)
{
    std::map<T, uint32_t> sum;

    for (auto d : dicts)
    {
        sum.merge(d);
    }

    for (auto& p : sum)
    {
        p.second = 0;
    }

    for (auto& d : dicts)
    {
        for (auto& p : d)
        {
            sum[p.first] += p.second;
        }
    }

    return sum;
}

template<typename H>
std::vector<size_t> bucket_depths(const H& hashmap)
{
    std::vector<size_t> depths(hashmap.bucket_count(), 0);
    for (size_t i = 0; i < hashmap.bucket_count(); i++)
    {
        depths[i] = hashmap.bucket_depth(i);
    }
    return depths;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

struct TestResults
{
    TestResults() = default;

    TestResults(size_t reserve)
    {
        fill_times.reserve(reserve);
        access_times.reserve(reserve);
        clear_times.reserve(reserve);
        collisions.reserve(reserve);
        depth_distributions.reserve(reserve);
    };

    std::vector<std::chrono::microseconds> fill_times;
    std::vector<std::chrono::microseconds> access_times;
    std::vector<std::chrono::microseconds> clear_times;
    std::vector<uint32_t> collisions;
    std::vector<std::map<size_t, uint32_t>> depth_distributions;
};

template<typename K, typename V, typename C>
static std::chrono::microseconds fill(const std::vector<K>& keys, const std::vector<V>& values, C& container)
{
    const auto begin = std::chrono::steady_clock::now();

    // fill container
    for (size_t i = 0; i < keys.size(); i++)
    {
        container.insert({ keys[i], values[i] });
    }

    const auto duration = std::chrono::steady_clock::now() - begin;
    return std::chrono::duration_cast<std::chrono::microseconds>(duration);
}

template<typename K, typename V, typename C>
static std::chrono::microseconds access(const std::vector<K>& keys, const std::vector<V>& values, C& container)
{
    const auto begin = std::chrono::steady_clock::now();

    // access container
    for (size_t i = 0; i < keys.size(); i++)
    {
        if (container.at(keys[i]) != values[i])
        {
            throw std::logic_error("key access error, mismatch");
        }
    }

    const auto duration = std::chrono::steady_clock::now() - begin;
    return std::chrono::duration_cast<std::chrono::microseconds>(duration);
}

template<typename K, typename V, typename C>
static std::chrono::microseconds clear(const std::vector<K>& keys, const std::vector<V>& values, C& container)
{
    const auto begin = std::chrono::steady_clock::now();

    // clear container
    /*for (size_t i = 0; i < keys.size(); i++)
    {
        if (container.erase(keys[i]) != values[i])
        {
            throw std::logic_error("key erase error");
        }
    }*/

    const auto duration = std::chrono::steady_clock::now() - begin;
    return std::chrono::duration_cast<std::chrono::microseconds>(duration);
}

template<typename C, typename G, typename... Args>
TestResults test(size_t repeats, G& fn_dataset_generator, Args&&... args)
{
    TestResults results(repeats);

    for (size_t r = 0; r < repeats; r++)
    {
        // create new unordered map and generate a new dataset
        C subject(std::forward<Args>(args)...);
        auto [data_x, data_y] = fn_dataset_generator(r);

        // fill test
        results.fill_times.push_back(fill(data_x, data_y, subject));

        // access test
        std::shuffle(data_x.begin(), data_x.end(), std::mt19937(0));
        std::shuffle(data_y.begin(), data_y.end(), std::mt19937(0));
        results.access_times.push_back(access(data_x, data_y, subject));

        // collect metadata results
        if constexpr (!std::is_same<C, std::unordered_map<typename C::key_type, typename C::mapped_type>>::value)
        {
            results.collisions.push_back(subject.collision_count());
            results.depth_distributions.push_back(count(bucket_depths(subject)));
        }

        // clear test
        std::shuffle(data_x.begin(), data_x.end(), std::mt19937(0));
        std::shuffle(data_y.begin(), data_y.end(), std::mt19937(0));
        results.clear_times.push_back(clear(data_x, data_y, subject));
    }

    return results;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
    srand(0);
    using key_type = uint64_t;
    using mapped_type = std::string;

    // parse input variables
    if (argc < 5)
    {
        std::cout << "help: " << argv[0] << " <map name> <repeats> <dataset size> <load factor>" << std::endl;
        return 1;
    }

    const MapType map_type = map_type_from_name(argv[1]);
    const size_t repeats = std::stoull(argv[2]);
    const size_t dataset_size = std::stoull(argv[3]);
    const float load_factor = std::stof(argv[4]);

    const size_t bucket_count = find_next_prime(dataset_size / load_factor);
    const auto datagen = [=](uint64_t seed) {
        return std::make_tuple(
            generate_data<key_type>::random(dataset_size, seed),
            generate_data<mapped_type>::random(dataset_size, seed)
        );
    };

    std::cout << "repeats: " << repeats << std::endl
              << "dataset size: " << dataset_size << std::endl
              << "load factor: " << load_factor << std::endl
              << "bucket count: " << bucket_count << std::endl;

    // select test
    const auto begin = std::chrono::steady_clock::now();
    TestResults results;
    switch (map_type)
    {
        case STD_UNORDERED:
            std::cout << "Testing: " << type_name<std::unordered_map<key_type, mapped_type>>() << std::endl;
            results = test<std::unordered_map<key_type, mapped_type>>(repeats, datagen, bucket_count);
            break;
/*
        case LINKED_LIST:
            std::cout << "Testing: " << type_name<LinkedListHashMap<value_type, value_type>>() << std::endl;
            results = test<LinkedListHashMap<value_type, value_type>>(repeats, datagen, bucket_count);
            break;

        case VECTOR:
            std::cout << "Testing: " << type_name<VectorHashMap<value_type, value_type>>() << std::endl;
            results = test<VectorHashMap<value_type, value_type>>(repeats, datagen, bucket_count);
            break;

        case CACHE_AWARE:
            std::cout << "Testing: " << type_name<CacheAwareHashMap<value_type, value_type>>() << std::endl;
            results = test<CacheAwareHashMap<value_type, value_type>>(repeats, datagen, bucket_count);
            break;

        case OPEN_ADDR:
            std::cout << "Testing: " << type_name<OpenAddressingHashMap<value_type, value_type>>() << std::endl;
            results = test<OpenAddressingHashMap<value_type, value_type>>(repeats, datagen, bucket_count);
            break;
*/
        case OASIMD:
            std::cout << "Testing: " << type_name<OASIMDHashMap<key_type, mapped_type>>() << std::endl;
            results = test<OASIMDHashMap<key_type, mapped_type>>(repeats, datagen, bucket_count);
            break;

        case AOS:
            std::cout << "Testing: " << type_name<AlignedOASIMDHashMap<key_type, mapped_type>>() << std::endl;
            results = test<AlignedOASIMDHashMap<key_type, mapped_type>>(repeats, datagen, bucket_count);
            break;

        default:
            return 0;
    }
    const auto duration = std::chrono::steady_clock::now() - begin;

    // calculate timings
    auto fill_total = std::accumulate(results.fill_times.begin(), results.fill_times.end(), std::chrono::microseconds{ 0 });
    auto access_total = std::accumulate(results.access_times.begin(), results.access_times.end(), std::chrono::microseconds{ 0 });
    auto clear_total = std::accumulate(results.clear_times.begin(), results.clear_times.end(), std::chrono::microseconds{ 0 });
    auto fill_average = fill_total / results.fill_times.size();
    auto access_average = access_total / results.access_times.size();
    auto clear_average = clear_total / results.clear_times.size();
    auto insert_average = fill_average.count() / (float)dataset_size;
    auto lookup_average = access_average.count() / (float)dataset_size;
    auto erase_average = clear_average.count() / (float)dataset_size;
    auto collision_total = std::accumulate(results.collisions.begin(), results.collisions.end(), 0);
    auto depth_dist_sum = merge_sum(results.depth_distributions);

    //TODO: depths are broken

    constexpr unsigned int COL = 16;
    std::cout << std::setw(8)    << std::left << ""
              << std::setw(COL) << std::right << "test min (us)" << ","
              << std::setw(COL) << std::right << "test max (us)" << ","
              << std::setw(COL) << std::right << "test avg (us)" << ","
              << std::setw(COL) << std::right << "call avg (us)" << ","
              << std::setw(COL) << std::right << "total (s)" << "," << std::endl;

    std::cout << std::setw(8)   << std::left << "Fill:"
              << std::setw(COL) << std::right << (*std::min_element(results.fill_times.begin(), results.fill_times.end())).count() << ","
              << std::setw(COL) << std::right << (*std::max_element(results.fill_times.begin(), results.fill_times.end())).count() << ","
              << std::setw(COL) << std::right << fill_average.count() << ","
              << std::setw(COL) << std::right << insert_average << ","
              << std::setw(COL) << std::right << fill_total.count() / 1.0e6 << "," << std::endl;

    std::cout << std::setw(8)   << std::left << "Access:"
              << std::setw(COL) << std::right << (*std::min_element(results.access_times.begin(), results.access_times.end())).count() << ","
              << std::setw(COL) << std::right << (*std::max_element(results.access_times.begin(), results.access_times.end())).count() << ","
              << std::setw(COL) << std::right << access_average.count() << ","
              << std::setw(COL) << std::right << lookup_average << ","
              << std::setw(COL) << std::right << access_total.count() / 1.0e6 << "," << std::endl;

    std::cout << std::setw(8)   << std::left << "Clear:"
              << std::setw(COL) << std::right << (*std::min_element(results.clear_times.begin(), results.clear_times.end())).count() << ","
              << std::setw(COL) << std::right << (*std::max_element(results.clear_times.begin(), results.clear_times.end())).count() << ","
              << std::setw(COL) << std::right << clear_average.count() << ","
              << std::setw(COL) << std::right << erase_average << ","
              << std::setw(COL) << std::right << clear_total.count() / 1.0e6 << "," << std::endl;

    std::cout << "Metadata: " << std::endl
              << "\t\tcollisions: " << collision_total << ", " << results.collisions << std::endl
              << "\t\tdepths: " << depth_dist_sum << std::endl;

    std::cout << "Duration: " << std::chrono::duration_cast<std::chrono::seconds>(duration).count() << "s" << std::endl;
    return 0;
}
