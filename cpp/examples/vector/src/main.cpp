#include <iostream>
#include <iomanip>
#include <cstring>
#include <random>
#include <chrono>
#include <vector>

#include "ostream_helpers.hpp"
#include "simd_wrapper.hpp"
#include "vec/simple_vec.hpp"
#include "vec/simd_vec.hpp"
#include "vec/dot_product.hpp"

using namespace sandbox;


/*template<typename T>
static std::ostream& operator<<(std::ostream& os, const std::vector<T>& v) 
{
    os << "[";
    for (auto itr = v.begin(); itr != v.end(); itr++)
    {
        os << " " << *itr;
    }
    os << " ]";
    return os;
}*/

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

template<typename I>
void fill(I begin, I end, uint32_t seed)
{
    std::mt19937 eng(seed);
    std::uniform_real_distribution<float> distribution;

    for (auto itr = begin; itr != end; itr++)
    {
        *itr = (float)distribution(eng);
    }
}

struct TestResults
{
    std::vector<uint64_t> durations;
    std::vector<float> products;
};

template<typename Fn>
TestResults test(size_t repeats, size_t dataset_size, Fn& fn_dot)
{
    TestResults results;

    float* a = new float[dataset_size];
    float* b = new float[dataset_size];

    for (size_t r = 0; r < repeats; r++)
    {
        // generate new vectors
        fill(a, a + dataset_size, r);
        fill(b, b + dataset_size, r + repeats);

        // perform timed test
        const auto begin = std::chrono::steady_clock::now();
        const float product = fn_dot(a, b, dataset_size);
        const auto duration = std::chrono::steady_clock::now() - begin;

        // record results
        results.durations.push_back(std::chrono::duration_cast<std::chrono::microseconds>(duration).count());
        results.products.push_back(product);
    }

    // clean up
    delete a;
    delete b;

    return results;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

int main()
{
/*
    constexpr size_t size = 32;
    std::array<float, size> a;
    std::array<float, size> b;

    for (size_t i = 0; i < size; i++)
    {
        a[i] = rand() % 10;
        b[i] = rand() % 10;
    }

    std::cout << a << std::endl;
    std::cout << b << std::endl;

    std::cout << dot_product_scalar(a, b, size) << std::endl;
    std::cout << dot_product_simd<float, 4>(a, b, size) << std::endl;
    std::cout << dot_product_simd<float, 8>(a, b, size) << std::endl;

    return 0;
*/

/*
    simple_vec<float, 4> a(1.0f, 2.0f, 3.0f, 4.0f);
    simple_vec<float, 8> b(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f);

    simd_vec<float, 4> c(1.0f, 2.0f, 3.0f, 4.0f);
    simd_vec<float, 8> d(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f);

    std::cout << a << " " << b << std::endl;
    std::cout << a.length_squared() << " " << b.length_squared() << std::endl;
    std::cout << dot(a, a) << " " << dot(b, b) << std::endl;

    std::cout << c << " " << d << std::endl;
    std::cout << c.length_squared() << " " << d.length_squared() << std::endl;
    std::cout << dot(c, c) << " " << dot(d, d) << std::endl;

    return 0;
*/

    constexpr size_t repeats = 8;
    constexpr size_t dataset_size = 32 * 1023;

    std::cout << "repeats: " << repeats << std::endl
              << "dataset size: " << dataset_size << std::endl
              << "vector size (bytes): " << (dataset_size * sizeof(float)) / 1024.0 << "KB" << std::endl;

    const TestResults results_linear = test(repeats, dataset_size, dot_product_scalar);
    const TestResults results_simd4 = test(repeats, dataset_size, dot_product_simd<float, 4>);
    const TestResults results_simd8 = test(repeats, dataset_size, dot_product_simd<float, 8>);

    std::cout << "Linear: " << std::endl
              << "\tdurations (us): " << results_linear.durations << std::endl
              << "\tproducts: " << results_linear.products << std::endl;

    std::cout << "SIMD 4: " << std::endl
              << "\tdurations (us): " << results_simd4.durations << std::endl
              << "\tproducts: " << results_simd4.products << std::endl;

    std::cout << "SIMD 8: " << std::endl
              << "\tdurations (us): " << results_simd8.durations << std::endl
              << "\tproducts: " << results_simd8.products << std::endl;

    return 0;
}

