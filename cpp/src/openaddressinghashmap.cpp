#include "hashmap/openaddressinghashmap.hpp"
#include <cstring>

#include <iostream>

namespace sandbox
{


//
template class OpenAddressingHashMap<std::string, std::string>;
template class OpenAddressingHashMap<uint64_t, uint64_t>;
template class OpenAddressingHashMap<uint32_t, uint32_t>;
template class OpenAddressingHashMap<uint16_t, uint16_t>;
template class OpenAddressingHashMap<uint8_t, uint8_t>;


template<typename K, typename M>
OpenAddressingHashMap<K, M>::OpenAddressingHashMap(size_t table_size)
    : m_table_size(table_size) //(max_elements / N_PREFETCHED_KEYS) + 1)
{
    // allocate bucket table
    //m_table = new HashMapEntry<K, M>[m_table_size];
    //m_table_occupied = new bool[m_table_size];
    //std::memset(m_table_occupied, false, m_table_size);

    //m_keys = new std::unique_ptr<KeyNode>[m_table_size]();
    //m_values = new std::unique_ptr<ValueNode>[m_table_size]();
    //std::cout << (int)N_PREFETCHED_KEYS << " (" << sizeof(K[N_PREFETCHED_KEYS]) << " " << sizeof(std::bitset<N_PREFETCHED_KEYS>) << ") " << sizeof(KeyNode) << std::endl;

    m_keys = new KeyNode[m_table_size]();
    m_values = new M[m_table_size]();
}

template<typename K, typename M>
OpenAddressingHashMap<K, M>::~OpenAddressingHashMap()
{
    // delete bucket table
    //delete[] m_table;
    //delete[] m_table_occupied;

    delete[] m_keys;
    delete[] m_values;
}

template<typename K, typename M>
void OpenAddressingHashMap<K, M>::insert(HashMapEntry<K, M>&& entry)
{
/*
    const size_t offset = this->bucket_index(entry.key, m_table_size);
    for (size_t i = 0; i < m_table_size; i++)
    {
        const size_t index = (offset + i) % m_table_size;  // wrap around index

        //std::cout << offset << " " << i << " " << index << ", " << m_table_occupied[i] << std::endl;


        if (!m_table_occupied[index])
        {
            //std::cout << search_counter << std::endl;

            // record key collision
            this->m_collisions += (int)(index != offset);

            // insert entry into unoccupied cell
            m_table_occupied[index] = true;
            m_table[index] = std::move(entry);
            return;
        }

        //search_counter++;
    }
*/


    /*for (size_t i = this->bucket_index(entry.key, m_table_size); i < m_table_size; i++)
    {
        if (!m_table_occupied[i])
        {
            // insert entry into unoccupied cell
            m_table_occupied[i] = true;
            m_table[i] = std::move(entry);
            break;
        }
    }*/


    //std::cout << (int)N_PREFETCHED_KEYS << " " << m_table_size << std::endl;

    const size_t offset = this->bucket_index(entry.key, m_table_size);
    for (size_t t = 0; t < m_table_size; t++)
    {
        const size_t index = (offset + t) % m_table_size;  // wrap around index

        //std::cout << offset << " " << t << " " << index << ", " << (bool)(m_keys[index] == nullptr)<< std::endl;

        /*if (m_keys[index] == nullptr)
        {
            m_keys[index] = std::make_unique<KeyNode>();
            m_values[index] = std::make_unique<ValueNode>();
        }*/

        /*for (uint8_t k = 0; k < N_PREFETCHED_KEYS; k++)
        {
            //std::cout << "\t" << (int)k << " " << m_keys[index]->occupied[k] << std::endl;

            if (!m_keys[index]->occupied[k])
            {
                m_keys[index]->occupied[k] = true;
                m_keys[index]->key[k] = entry.key;
                m_values[index]->value[k] = entry.value;

                //std::cout << search_counter << std::endl;
                return;
            }
        }*/

        /*KeyNode* node = m_keys[index].get();
        for (uint8_t k = 0; k < N_PREFETCHED_KEYS; k++)
        {
            //std::cout << "\t" << (int)k << " " << node->occu.test(k) << std::endl;

            if (!node->occu.test(k))
            {
                node->occu.set(k, true);
                node->key[k] = entry.key;
                m_values[index]->value[k] = entry.value;

                //
                this->m_collisions += (uint64_t)(k != 0);

                return;
            }
        }*/

        if (!m_keys[index].occupied)
        {
            // record key collision
            this->m_collisions += (int)(index != offset);

            // insert
            m_keys[index].occupied = true;
            m_keys[index].key = entry.key;
            m_values[index] = entry.value;
            return;
        }
    }

    throw std::out_of_range("free cell not found");
}

template<typename K, typename M>
M OpenAddressingHashMap<K, M>::erase(const K& key)
{
    return (M)0; // TODO: temporary
}

template<typename K, typename M>
M& OpenAddressingHashMap<K, M>::at(const K& key)
{
/*
    const size_t offset = this->bucket_index(key, m_table_size);
    for (size_t i = 0; i < m_table_size; i++)
    {
        const size_t index = (offset + i) % m_table_size;  // wrap around index

        if (!m_table_occupied[index])
        {
            break;
        }
        else if (m_table[index].key == key)
        {
            return m_table[index].value;
        }
    }
*/
/*
    const size_t offset = this->bucket_index(key, m_table_size);
    for (size_t t = 0; t < m_table_size; t++)
    {
        const size_t index = (offset + t) % m_table_size;  // wrap around index

        /*if (m_keys[index] == nullptr)
        {
            break;
        }*/

        /*for (uint8_t k = 0; k < N_PREFETCHED_KEYS; k++)
        {
            if (m_keys[index]->occupied[k] && m_keys[index]->key[k] == key)
            {
                return m_values[index]->value[k];
            }
        }*/

        /*KeyNode* node = m_keys[index].get();
        for (uint8_t k = 0; (node->occu.to_ullong() >> k) > 0U; k++)
        {
            if (node->occu[k] && node->key[k] == key)
            {
                return m_values[index]->value[k];
            }
        }*/

/*
        if (!m_keys[index].occupied)
        {
            break;
        }

        if (m_keys[index].key == key)
        {
            return m_values[index];
        }
    }
*/

    const size_t offset = this->bucket_index(key, m_table_size);
    for (size_t index = offset; index < m_table_size; index++)
    {
        if (!m_keys[index].occupied)
        {
            break;
        }

        if (m_keys[index].key == key)
        {
            return m_values[index];
        }
    }

    for (size_t index = 0; index < offset; index++)
    {
        if (!m_keys[index].occupied)
        {
            break;
        }

        if (m_keys[index].key == key)
        {
            return m_values[index];
        }
    }


    throw std::out_of_range("key not found");
}

template<typename K, typename M>
size_t OpenAddressingHashMap<K, M>::bucket_count() const
{
    return m_table_size;
}

template<typename K, typename M>
size_t OpenAddressingHashMap<K, M>::bucket_depth(size_t index) const
{
    return 1; //N_PREFETCHED_KEYS;
}

template<typename K, typename M>
OpenAddressingHashMap<K, M>::KeyNode::KeyNode()
    : occupied(false)
{
    //std::memset(occupied, false, N_PREFETCHED_KEYS);
}


}
