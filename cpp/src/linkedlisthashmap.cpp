#include "hashmap/linkedlisthashmap.hpp"

namespace sandbox
{


//
template class LinkedListHashMap<std::string, std::string>;
template class LinkedListHashMap<uint64_t, uint64_t>;
template class LinkedListHashMap<uint32_t, uint32_t>;
template class LinkedListHashMap<uint16_t, uint16_t>;
template class LinkedListHashMap<uint8_t, uint8_t>;

template<typename K, typename M>
LinkedListHashMap<K, M>::LinkedListHashMap(size_t table_size)
    : m_table_size(table_size)
{
    // allocate bucket table
    m_table = new std::unique_ptr<Node>[m_table_size];
}

template<typename K, typename M>
LinkedListHashMap<K, M>::~LinkedListHashMap()
{
    // delete bucket table
    delete[] m_table;
}

template<typename K, typename M>
void LinkedListHashMap<K, M>::insert(HashMapEntry<K, M>&& entry)
{
    const size_t idx = this->bucket_index(entry.key, m_table_size);
    Node* parent = nullptr;
    Node* node = this->m_table[idx].get();

    //
    if (node != nullptr)
    {
        this->m_collisions++;
    }

    // traverse to end of linked list or until key match found
    while (node != nullptr && node->entry.key != entry.key)
    {
        parent = node;
        node = node->next.get();
    }

    if (node != nullptr)
    {
        // modify existing entry
        node->entry.value = entry.value;
    }
    else
    {
        // create new node
        auto new_node = std::make_unique<Node>(std::move(entry));
        if (parent != nullptr)
        {
            parent->next = std::move(new_node);
        }
        else
        {
            this->m_table[idx] = std::move(new_node);
        }
    }
}

template<typename K, typename M>
M LinkedListHashMap<K, M>::erase(const K& key)
{
    return (M)0; //TODO: temporary
/*
    std::unique_ptr<Node>* parent = this->;
    Node* node = this->m_table[this->bucket_index(key, m_table_size)].get();
    while (node != nullptr)
    {
        if (node->entry.key == key)
        {

            


            return node->entry.value;
        }
        node = node->next.get();
    }


    throw std::out_of_range("key not found");
*/
}

template<typename K, typename M>
M& LinkedListHashMap<K, M>::at(const K& key)
{
    Node* node = this->m_table[this->bucket_index(key, m_table_size)].get();

    while (node != nullptr)
    {
        if (node->entry.key == key)
        {
            return node->entry.value;
        }
        node = node->next.get();
    }

    throw std::out_of_range("key not found");
}

template<typename K, typename M>
size_t LinkedListHashMap<K, M>::bucket_count() const
{
    return m_table_size;
}

template<typename K, typename M>
size_t LinkedListHashMap<K, M>::bucket_depth(size_t index) const
{
    if (m_table[index] == nullptr)
    {
        return 0;
    }
    return m_table[index]->size();
}

template<typename K, typename M>
LinkedListHashMap<K, M>::Node::Node(HashMapEntry<K, M>&& entry)
    : entry(entry), next(nullptr)
{
}

template<typename K, typename M>
size_t LinkedListHashMap<K, M>::Node::size() const
{
    return 1 + (next == nullptr ? 0 : next->size());
}


}
