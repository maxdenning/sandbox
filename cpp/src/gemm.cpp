#include "mat/gemm.hpp"
#include <algorithm>
#include <immintrin.h>
#include <omp.h>

#define ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c) \
    if (!are_dimensions_compatible((a), (b), (c))) { \
        throw std::invalid_argument("Incompatible matrix dimensions"); }

namespace sandbox
{


static constexpr size_t L2_CACHE_SIZE = 262144;  // 256kb
static constexpr size_t CACHE_LINE_SIZE = 64;  // 64b

void gemm_ijp(const MatrixD& a, const MatrixD& b, MatrixD& c)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c)

    for (size_t i = 0; i < c.rows(); i++)
    {
        for (size_t j = 0; j < c.cols(); j++)
        {
            for (size_t p = 0; p < a.cols(); p++)
            {
                c(i, j) += a(i, p) * b(p, j);
            }
        }
    }
}

void gemm_ipj(const MatrixD& a, const MatrixD& b, MatrixD& c)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c)

    for (size_t i = 0; i < c.rows(); i++)
    {
        for (size_t p = 0; p < a.cols(); p++)
        {
            for (size_t j = 0; j < c.cols(); j++)
            {
                c(i, j) += a(i, p) * b(p, j);
            }
        }
    }
}

void gemm_jip(const MatrixD& a, const MatrixD& b, MatrixD& c)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c)

    for (size_t j = 0; j < c.cols(); j++)
    {
        for (size_t i = 0; i < c.rows(); i++)
        {
            for (size_t p = 0; p < a.cols(); p++)
            {
                c(i, j) += a(i, p) * b(p, j);
            }
        }
    }
}

void gemm_jpi(const MatrixD& a, const MatrixD& b, MatrixD& c)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c)

    for (size_t j = 0; j < c.cols(); j++)
    {
        for (size_t p = 0; p < a.cols(); p++)
        {
            for (size_t i = 0; i < c.rows(); i++)
            {
                c(i, j) += a(i, p) * b(p, j);
            }
        }
    }
}

void gemm_pij(const MatrixD& a, const MatrixD& b, MatrixD& c)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c)

    for (size_t p = 0; p < a.cols(); p++)
    {
        for (size_t i = 0; i < c.rows(); i++)
        {
            for (size_t j = 0; j < c.cols(); j++)
            {
                c(i, j) += a(i, p) * b(p, j);
            }
        }
    }
}

void gemm_pji(const MatrixD& a, const MatrixD& b, MatrixD& c)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c)

    for (size_t p = 0; p < a.cols(); p++)
    {
        for (size_t j = 0; j < c.cols(); j++)
        {
            for (size_t i = 0; i < c.rows(); i++)
            {
                c(i, j) += a(i, p) * b(p, j);
            }
        }
    }
}

void gemm_gemv_jpi(const MatrixD& a, const MatrixD& b, MatrixD& c)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c)

    for (size_t j = 0; j < c.cols(); j++)
    {
        gemv(a, &b(0, j), &c(0, j));
    }
}

void gemm_ger_pji(const MatrixD& a, const MatrixD& b, MatrixD& c)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c);

    /* partition A by columns and B by rows
       ( a0 | a1 | ... | aK-1 ) * ( b0   ) + C
                                  ( b1   )
                                  ( ...  )
                                  ( bK-1 )
    */

    for (size_t p = 0; p < a.cols(); p++)
    {
        // C := Ap * Bp^t + C               rank-1 update
        //     Cj := Bpj * Ap + Cj          axpy
        //         Cij := Bpj * Aip + Cij
        ger_ji(c, &a(0, p), &b(p, 0), b.ld());
    }
}

void gemm_ger_pij(const MatrixD& a, const MatrixD& b, MatrixD& c)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c);

    for (size_t p = 0; p < a.cols(); p++)
    {
        // C := Ap * Bp^t + C               rank-1 update
        //     Ci^t := Aip * Bp^t + Ci^t    axpy
        //         Cij := Aip * Bpj + Cij
        ger_ij(c, &a(0, p), &b(p, 0), b.ld());
    }
}

void gemm_block_ger_ji(const MatrixD& a, const MatrixD& b, MatrixD& c, size_t mb, size_t nb) //, size_t kb)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c)

    /*
        partition C into tiles of (mb x nb)
                  A into row panels of (mb x k)
                  B into column panels of (k x nb)
       
        perform a rank-1 update on panel rows and columns
    */

    for (size_t j = 0; j < c.cols(); j += nb)
    {
        const size_t jb = std::min(nb, c.cols() - j);  // account for fringe where tile is not full size

        for (size_t i = 0; i < c.rows(); i += mb)
        {
            const size_t ib = std::min(mb, c.rows() - i);  // account for fringe where tile is not full size

            // used when partitioning A and B into tiles instead of panels 
            /*for (size_t p = 0; p < a.cols(); p += kb)
            {
                const size_t pb = std::min(kb, a.cols() - p);  // account for fringe where tile is not full size

                auto c_block = c.view(ib, jb, i, j);
                gemm_ger_pji(a.view(ib, pb, i, p), b.view(pb, jb, p, j), c_block);
            }*/

            auto c_block = c.view(ib, jb, i, j);
            gemm_ger_pji(a.view(ib, a.cols(), i, 0), b.view(b.rows(), jb, 0, j), c_block);
        }
    }
}

void gemm_block_ger_ij(const MatrixD& a, const MatrixD& b, MatrixD& c, size_t mb, size_t nb) //, size_t kb)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c)

    /*
        partition C into tiles of (mb x nb)
                  A into row panels of (mb x k)
                  B into column panels of (k x nb)
       
        perform a rank-1 update on panel rows and columns
    */

    for (size_t j = 0; j < c.cols(); j += nb)
    {
        const size_t jb = std::min(nb, c.cols() - j);  // account for fringe where tile is not full size

        for (size_t i = 0; i < c.rows(); i += mb)
        {
            const size_t ib = std::min(mb, c.rows() - i);  // account for fringe where tile is not full size

            // used when partitioning A and B into tiles instead of panels 
            /*for (size_t p = 0; p < a.cols(); p += kb)
            {
                const size_t pb = std::min(kb, a.cols() - p);  // account for fringe where tile is not full size

                auto c_block = c.view(ib, jb, i, j);
                gemm_ger_pij(a.view(ib, pb, i, p), b.view(pb, jb, p, j), c_block);
            }*/

            auto c_block = c.view(ib, jb, i, j);
            gemm_ger_pij(a.view(ib, a.cols(), i, 0), b.view(b.rows(), jb, 0, j), c_block);
        }
    }
}

void gemm_block_ji_mbx4(const MatrixD& a, const MatrixD& b, MatrixD& c, size_t mb)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c)

    /*
        Partion C into tiles (4x4), A into row panels (4xk), and B into column panels (kx4)
        Run a kernel over each A-B panel pair to update a single tile of C
    */

    constexpr size_t nb = 4;

    // only allow multiples of block size
    if (c.rows() % mb != 0 || c.cols() % nb != 0)
    {
        throw std::invalid_argument("Matrix dimensions must be a divisible by block size");
    }

    // select kernel
    FnKernel fn_kernel =  get_kernel(mb);

    //
    for (size_t j = 0; j < c.cols(); j += nb)
    {
        for (size_t i = 0; i < c.rows(); i += mb)
        {
            fn_kernel(a.cols(), &a(i, 0), a.ld(), &b(0, j), b.ld(), &c(i, j), c.ld());
        }
    }
}

void gemm_cached_pi_ji_mbx4(const MatrixD& a, const MatrixD& b, MatrixD& c, size_t mb)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c)

    /*
        Matrix sizes: m, n, k
        Tiles and panel sizes (chunks): mc, kc
        Micro-tile and micro-panel sizes (blocks): mb, nb

        Partition C (m x n) into row panels (mc x n),
                  A (m x k) into tiles (mc x kc),
                  B (k x n) into row panels (kc x n)
        
        Partition C panels into micro-tiles (mb x nb),
                  A tiles into row micro-panels (mb x kc),
                  B panels into column micro-panels (kc x nb)


        This means only a micro-tile of C, a tile of A, and a micro-panel of B
        need to be loaded into cache at any one time.

        The tile of A can remain in cache while the smaller micro-panels of C
        and B are each loaded/unloaded only once.
    */

    constexpr size_t mc = 96;  // must be multiple of mb
    constexpr size_t kc = 96;
    constexpr size_t nb = 4;

    // only allow multiples of block size
    if (c.rows() % mb != 0 || c.cols() % nb != 0)
    {
        throw std::invalid_argument("Matrix dimensions must be a divisible by block size");
    }

    // select kernel
    FnKernel fn_kernel = get_kernel(mb);

    // loop over PI (the indices of A), so tiles of A remain in cache 
    for (size_t p = 0; p < a.cols(); p += kc)
    {
        const size_t pc = std::min(kc, a.cols() - p);  // fringe

        for (size_t i = 0; i < c.rows(); i += mc)
        {
            const size_t ic = std::min(mc, c.rows() - i);  // fringe

            // inner loop over JI (the indices of C), so micro-tiles of C can be updated
            kernel_ji_mbx4(fn_kernel, mb, ic, c.cols(), pc, &a(i, p), a.ld(), &b(p, 0), b.ld(), &c(i, 0), c.ld());
        }
    }
}

void gemm_cached_jpi_ji_12x4(const MatrixD& a, const MatrixD& b, MatrixD& c)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c)

    /*
        Matrix sizes: m, n, k
        Tiles and panel sizes (chunks): mc, nc, kc
        Micro-tile and micro-panel sizes (blocks): mb, nb

        Loop 1: J
            Partition B and C into column panels
            Bj (k x nc) (L3 cache)
            Cj (m x nc)

        Loop 2 & 3: PI
            Partition A into tiles and B and C into row panels
            Aip (mc x kc) (L2 cache)
            Bpj (kc x nc) (L3 cache)
            Cij (mc x nc)

        Loop 4 & 5: JI
            Partition A into row micro-panels, B into column micro-panels, and C into micro-tiles
            Aip_i  (mb x kc) (L2 cache)
            Bpj_j  (kc x nb) (L1 cache)
            Cij_ij (mb x nb)
        
        Kernel: P
            Partition A into column vectors and B into row vectors
            a_p  (mb x 1)
            b_p  (1 x nb)
            c    (mb x nb)
    */

    constexpr size_t mc = 96;  // must be multiple of mb
    constexpr size_t nc = 96;  // must be multiple of nb
    constexpr size_t kc = 96;
    constexpr size_t mb = 12;
    constexpr size_t nb = 4;

    // only allow multiples of block size
    if (c.rows() % mb != 0 || c.cols() % nb != 0)
    {
        throw std::invalid_argument("Matrix dimensions must be a divisible by block size");
    }

    // J
    for (size_t j = 0; j < c.cols(); j += nc)
    {
        const size_t jc = std::min(nc, c.cols() - j);  // fringe

        // PI
        for (size_t p = 0; p < a.cols(); p += kc)
        {
            const size_t pc = std::min(kc, a.cols() - p);  // fringe

            for (size_t i = 0; i < c.rows(); i += mc)
            {
                const size_t ic = std::min(mc, c.rows() - i);  // fringe

                // JI
                for (size_t j_ = 0; j_ < jc; j_ += nb)
                {
                    for (size_t i_ = 0; i_ < ic; i_ += mb)
                    {
                        // kernel
                        kernel_12x4(
                            pc,
                            &(&a(i, p))[RAVEL_INDEX(i_, 0, a.ld())],
                            a.ld(),
                            &(&b(p, j))[RAVEL_INDEX(0, j_, b.ld())],
                            b.ld(),
                            &(&c(i, j))[RAVEL_INDEX(i_, j_, c.ld())],
                            c.ld()
                        );
                    }
                }
            }
        }
    }
}


void gemm_packed_jpi_ji_12x4(const MatrixD& a, const MatrixD& b, MatrixD& c)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c)

    /*
        Matrix sizes: m, n, k
        Tiles and panel sizes (chunks): mc, nc, kc
        Micro-tile and micro-panel sizes (blocks): mb, nb

        Loop 1: J
            Partition B and C into column panels
            Bj (k x nc) (L3 cache)
            Cj (m x nc)

        Loop 2 & 3: PI
            Partition A into tiles and B and C into row panels
            Aip (mc x kc) (L2 cache)
            Bpj (kc x nc) (L3 cache)
            Cij (mc x nc)

            2: Bpj is packed into a buffer of contiguous column micro-panels (B~pj)
            3: Aip is packed into a buffer of contiguous row micro-panels (A~ip)

        Loop 4 & 5: JI
            Partition C into micro-tiles.
            Select packed row micro-panel from A~ and packed column micro-panel from B~. 
            A~ip_i  (mb x kc) (L2 cache)
            B~pj_j  (kc x nb) (L1 cache)
            Cij_ij (mb x nb)

        Kernel: P
            Partition A~ into column vectors and B~ into row vectors
            a_p  (mb x 1)
            b_p  (1 x nb)
            c    (mb x nb)
    */

    constexpr size_t mc = 96;  // must be multiple of mb
    constexpr size_t nc = 2016;  // must be multiple of nb
    constexpr size_t kc = 96;
    constexpr size_t mb = 12;
    constexpr size_t nb = 4;

    // only allow multiples of block size
    if (c.rows() % mb != 0 || c.cols() % nb != 0)
    {
        throw std::invalid_argument("Matrix dimensions must be a divisible by block size");
    }

    // allocate A~ and B~ packed buffers
    double* const a_packed = (double*)_mm_malloc(mc * kc * sizeof(double), CACHE_LINE_SIZE);
    double* const b_packed = (double*)_mm_malloc(kc * nc * sizeof(double), CACHE_LINE_SIZE);

    // J
    for (size_t j = 0; j < c.cols(); j += nc)
    {
        const size_t jc = std::min(nc, c.cols() - j);  // fringe

        // PI
        for (size_t p = 0; p < a.cols(); p += kc)
        {
            const size_t pc = std::min(kc, a.cols() - p);  // fringe
            pack_row_panel(pc, jc, nb, &b(p, j), b.ld(), b_packed);  // pack Bpj into B~pj

            for (size_t i = 0; i < c.rows(); i += mc)
            {
                const size_t ic = std::min(mc, c.rows() - i);  // fringe
                pack_tile(ic, pc, mb, &a(i, p), a.ld(), a_packed);  // pack Aip into A~ip

                // JI
                for (size_t j_ = 0; j_ < jc; j_ += nb)
                {
                    for (size_t i_ = 0; i_ < ic; i_ += mb)
                    {
                        // kernel
                        kernel_packed_12x4(
                            pc,
                            &a_packed[i_ * pc],
                            &b_packed[pc * j_],
                            &(&c(i, j))[RAVEL_INDEX(i_, j_, c.ld())],
                            c.ld()
                        );
                    }
                }
            }
        }
    }

    // clean up
    _mm_free(a_packed);
    _mm_free(b_packed);
}

void gemm_mp_ijp(const MatrixD& a, const MatrixD& b, MatrixD& c)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c)

    #pragma omp parallel for
    for (size_t i = 0; i < c.rows(); i++)
    {
        for (size_t j = 0; j < c.cols(); j++)
        {
            for (size_t p = 0; p < a.cols(); p++)
            {
                c(i, j) += a(i, p) * b(p, j);
            }
        }
    }
}

void gemm_mp_jpi(const MatrixD& a, const MatrixD& b, MatrixD& c)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c)

    #pragma omp parallel for
    for (size_t j = 0; j < c.cols(); j++)
    {
        for (size_t p = 0; p < a.cols(); p++)
        {
            for (size_t i = 0; i < c.rows(); i++)
            {
                c(i, j) += a(i, p) * b(p, j);
            }
        }
    }
}

void gemm_mp_packed_jpi_ji_12x4(const MatrixD& a, const MatrixD& b, MatrixD& c)
{
    ASSERT_MATRIX_DIMS_COMPATIBLE(a, b, c)

    constexpr size_t mc = 96;  // must be multiple of mb
    constexpr size_t nc = 2016;  // must be multiple of nb
    constexpr size_t kc = 96;
    constexpr size_t mb = 12;
    constexpr size_t nb = 4;

    // each thread works on at most (nc / n_threads) columns of C and B
    const size_t nc_per_thread = (((nc / omp_get_max_threads()) / nb) * nb);  // must be multiple of nb
    const size_t remaining_cols = c.cols() % nc_per_thread;  // fringe of nc, which could cause load imbalance between threads
    const size_t load_balanced_cols = c.cols() - remaining_cols;  // size divisible by nc_per_thread
    const size_t remaining_per_thread = std::max(nb, (((remaining_cols / omp_get_max_threads()) / nb) * nb));  // must be multiple of nb

    // only allow multiples of block size
    if (c.rows() % mb != 0 || c.cols() % nb != 0)
    {
        throw std::invalid_argument("Matrix dimensions must be a divisible by block size");
    }

    //
    const auto inner_loop = [&](const size_t j, const size_t jc) {
        // allocate A~ and B~ packed buffers for each thread
        double* const a_packed = (double*)_mm_malloc(mc * kc * sizeof(double), CACHE_LINE_SIZE);
        double* const b_packed = (double*)_mm_malloc(kc * nc * sizeof(double), CACHE_LINE_SIZE);

        // PI
        for (size_t p = 0; p < a.cols(); p += kc)
        {
            const size_t pc = std::min(kc, a.cols() - p);  // fringe
            pack_row_panel(pc, jc, nb, &b(p, j), b.ld(), b_packed);  // pack Bpj into B~pj

            for (size_t i = 0; i < c.rows(); i += mc)
            {
                const size_t ic = std::min(mc, c.rows() - i);  // fringe
                pack_tile(ic, pc, mb, &a(i, p), a.ld(), a_packed);  // pack Aip into A~ip

                // JI
                for (size_t j_ = 0; j_ < jc; j_ += nb)
                {
                    for (size_t i_ = 0; i_ < ic; i_ += mb)
                    {
                        // kernel
                        kernel_packed_12x4(
                            pc,
                            &a_packed[i_ * pc],
                            &b_packed[pc * j_],
                            &(&c(i, j))[RAVEL_INDEX(i_, j_, c.ld())],
                            c.ld()
                        );
                    }
                }
            }
        }

        // clean up
        _mm_free(a_packed);
        _mm_free(b_packed);
    };

    // J
    #pragma omp parallel for
    for (size_t j = 0; j < load_balanced_cols; j += nc_per_thread)
    {
        inner_loop(j, nc_per_thread);
    }

    // J, remainder
    #pragma omp parallel for
    for (size_t j = load_balanced_cols; j < c.cols(); j += remaining_per_thread)
    {
        const size_t jc = std::min(remaining_per_thread, c.cols() - j);  // fringe
        inner_loop(j, jc);
    }
}

bool are_dimensions_compatible(const MatrixD& a, const MatrixD& b, const MatrixD& c)
{
    return a.cols() == b.rows() && a.rows() == c.rows() && b.cols() == c.cols();
}

FnKernel get_kernel(size_t mb)
{
    FnKernel fn_kernel = nullptr;
    switch (mb) {
        case 4:
            fn_kernel = kernel_4x4;
            break;

        case 8:
            fn_kernel = kernel_8x4;
            break;

        case 12:
            fn_kernel = kernel_12x4;
            break;

        default:
            throw std::invalid_argument("mb must be 4, 8, or 12");
    }
    return fn_kernel;
}

void axpy(double alpha, size_t m, const double* const __restrict__ x, double* const __restrict__ y, size_t x_ld, size_t y_ld)
{
    for (size_t i = 0; i < m; i++)
    {
        y[i * y_ld] += alpha * x[i * x_ld];
    }
}

void gemv(const MatrixD& a, const double* const __restrict__ x, double* const __restrict__ y)
{
    for (size_t p = 0; p < a.cols(); p++)
    {
        axpy(x[p], a.rows(), &a(0, p), y);
    }
}

void ger_ji(MatrixD& a, const double* const __restrict__ x, const double* const __restrict__ y, size_t y_ld)
{
    for (size_t j = 0; j < a.cols(); j++)
    {
        axpy(y[j * y_ld], a.rows(), x, &a(0, j));
    }
}

void ger_ij(MatrixD& a, const double* const __restrict__ x, const double* const __restrict__ y, size_t y_ld)
{
    for (size_t i = 0; i < a.rows(); i++)
    {
        axpy(x[i], a.cols(), y, &a(i, 0), y_ld, a.ld());
    }
}

void kernel_4x4(size_t k, const double* const __restrict__ a, size_t a_ld, const double* const __restrict__ b, size_t b_ld, double* const __restrict__ c, size_t c_ld)
{
    // load 4x4 C tile into vector registers
    __m256d c_col0 = _mm256_loadu_pd(&c[RAVEL_INDEX(0, 0, c_ld)]);
    __m256d c_col1 = _mm256_loadu_pd(&c[RAVEL_INDEX(0, 1, c_ld)]);
    __m256d c_col2 = _mm256_loadu_pd(&c[RAVEL_INDEX(0, 2, c_ld)]);
    __m256d c_col3 = _mm256_loadu_pd(&c[RAVEL_INDEX(0, 3, c_ld)]);

    // for each column/row vector pair in A and B
    for (size_t p = 0; p < k; p++)
    {
        //
        __m256d a_p = _mm256_loadu_pd(&a[RAVEL_INDEX(0, p, a_ld)]);  // load column p from A
        __m256d b_pj; // will contain value Bpj broadcast to vector

        //
        b_pj = _mm256_set1_pd(b[RAVEL_INDEX(p, 0, b_ld)]);
        c_col0 = _mm256_fmadd_pd(a_p, b_pj, c_col0);
        
        b_pj = _mm256_set1_pd(b[RAVEL_INDEX(p, 1, b_ld)]);
        c_col1 = _mm256_fmadd_pd(a_p, b_pj, c_col1);

        b_pj = _mm256_set1_pd(b[RAVEL_INDEX(p, 2, b_ld)]);
        c_col2 = _mm256_fmadd_pd(a_p, b_pj, c_col2);
        
        b_pj = _mm256_set1_pd(b[RAVEL_INDEX(p, 3, b_ld)]);
        c_col3 = _mm256_fmadd_pd(a_p, b_pj, c_col3);
    }

    // store 4x4 tile back to C
    _mm256_storeu_pd(&c[RAVEL_INDEX(0, 0, c_ld)], c_col0);
    _mm256_storeu_pd(&c[RAVEL_INDEX(0, 1, c_ld)], c_col1);
    _mm256_storeu_pd(&c[RAVEL_INDEX(0, 2, c_ld)], c_col2);
    _mm256_storeu_pd(&c[RAVEL_INDEX(0, 3, c_ld)], c_col3);
}

void kernel_8x4(size_t k, const double* const __restrict__ a, size_t a_ld, const double* const __restrict__ b, size_t b_ld, double* const __restrict__ c, size_t c_ld)
{
    // load 8x4 C tile into vector registers, each column as 4 upper values and 4 lower values
    __m256d c_col0_u = _mm256_loadu_pd(&c[RAVEL_INDEX(0, 0, c_ld)]);
    __m256d c_col1_u = _mm256_loadu_pd(&c[RAVEL_INDEX(0, 1, c_ld)]);
    __m256d c_col2_u = _mm256_loadu_pd(&c[RAVEL_INDEX(0, 2, c_ld)]);
    __m256d c_col3_u = _mm256_loadu_pd(&c[RAVEL_INDEX(0, 3, c_ld)]);
    __m256d c_col0_l = _mm256_loadu_pd(&c[RAVEL_INDEX(4, 0, c_ld)]);
    __m256d c_col1_l = _mm256_loadu_pd(&c[RAVEL_INDEX(4, 1, c_ld)]);
    __m256d c_col2_l = _mm256_loadu_pd(&c[RAVEL_INDEX(4, 2, c_ld)]);
    __m256d c_col3_l = _mm256_loadu_pd(&c[RAVEL_INDEX(4, 3, c_ld)]);

    // for each column/row vector pair in A and B
    for (size_t p = 0; p < k; p++)
    {
        //
        __m256d a_p_u = _mm256_loadu_pd(&a[RAVEL_INDEX(0, p, a_ld)]);  // load column p from A, upper 4 values
        __m256d a_p_l = _mm256_loadu_pd(&a[RAVEL_INDEX(4, p, a_ld)]);  // load column p from A, lower 4 values
        __m256d b_pj; // will contain value Bpj broadcast to vector

        //
        b_pj = _mm256_set1_pd(b[RAVEL_INDEX(p, 0, b_ld)]);
        c_col0_u = _mm256_fmadd_pd(a_p_u, b_pj, c_col0_u);
        c_col0_l = _mm256_fmadd_pd(a_p_l, b_pj, c_col0_l);
        
        b_pj = _mm256_set1_pd(b[RAVEL_INDEX(p, 1, b_ld)]);
        c_col1_u = _mm256_fmadd_pd(a_p_u, b_pj, c_col1_u);
        c_col1_l = _mm256_fmadd_pd(a_p_l, b_pj, c_col1_l);

        b_pj = _mm256_set1_pd(b[RAVEL_INDEX(p, 2, b_ld)]);
        c_col2_u = _mm256_fmadd_pd(a_p_u, b_pj, c_col2_u);
        c_col2_l = _mm256_fmadd_pd(a_p_l, b_pj, c_col2_l);
        
        b_pj = _mm256_set1_pd(b[RAVEL_INDEX(p, 3, b_ld)]);
        c_col3_u = _mm256_fmadd_pd(a_p_u, b_pj, c_col3_u);
        c_col3_l = _mm256_fmadd_pd(a_p_l, b_pj, c_col3_l);
    }

    // store 8x4 tile back to C
    _mm256_storeu_pd(&c[RAVEL_INDEX(0, 0, c_ld)], c_col0_u);
    _mm256_storeu_pd(&c[RAVEL_INDEX(0, 1, c_ld)], c_col1_u);
    _mm256_storeu_pd(&c[RAVEL_INDEX(0, 2, c_ld)], c_col2_u);
    _mm256_storeu_pd(&c[RAVEL_INDEX(0, 3, c_ld)], c_col3_u);
    _mm256_storeu_pd(&c[RAVEL_INDEX(4, 0, c_ld)], c_col0_l);
    _mm256_storeu_pd(&c[RAVEL_INDEX(4, 1, c_ld)], c_col1_l);
    _mm256_storeu_pd(&c[RAVEL_INDEX(4, 2, c_ld)], c_col2_l);
    _mm256_storeu_pd(&c[RAVEL_INDEX(4, 3, c_ld)], c_col3_l);
}

void kernel_12x4(size_t k, const double* const __restrict__ a, size_t a_ld, const double* const __restrict__ b, size_t b_ld, double* const __restrict__ c, size_t c_ld)
{
    // utilise all 16 vector registers - 12 to store C, 3 for A panel, 1 for B panel

    // load 12x4 C tile into vector registers, each column as 4 upper values, 4 middle values, and 4 lower values
    __m256d c_col0_u = _mm256_loadu_pd(&c[RAVEL_INDEX(0, 0, c_ld)]);
    __m256d c_col1_u = _mm256_loadu_pd(&c[RAVEL_INDEX(0, 1, c_ld)]);
    __m256d c_col2_u = _mm256_loadu_pd(&c[RAVEL_INDEX(0, 2, c_ld)]);
    __m256d c_col3_u = _mm256_loadu_pd(&c[RAVEL_INDEX(0, 3, c_ld)]);
    __m256d c_col0_m = _mm256_loadu_pd(&c[RAVEL_INDEX(4, 0, c_ld)]);
    __m256d c_col1_m = _mm256_loadu_pd(&c[RAVEL_INDEX(4, 1, c_ld)]);
    __m256d c_col2_m = _mm256_loadu_pd(&c[RAVEL_INDEX(4, 2, c_ld)]);
    __m256d c_col3_m = _mm256_loadu_pd(&c[RAVEL_INDEX(4, 3, c_ld)]);
    __m256d c_col0_l = _mm256_loadu_pd(&c[RAVEL_INDEX(8, 0, c_ld)]);
    __m256d c_col1_l = _mm256_loadu_pd(&c[RAVEL_INDEX(8, 1, c_ld)]);
    __m256d c_col2_l = _mm256_loadu_pd(&c[RAVEL_INDEX(8, 2, c_ld)]);
    __m256d c_col3_l = _mm256_loadu_pd(&c[RAVEL_INDEX(8, 3, c_ld)]);

    // for each column/row vector pair in A and B
    for (size_t p = 0; p < k; p++)
    {
        //
        __m256d a_p_u = _mm256_loadu_pd(&a[RAVEL_INDEX(0, p, a_ld)]);  // load column p from A, upper 4 values
        __m256d a_p_m = _mm256_loadu_pd(&a[RAVEL_INDEX(4, p, a_ld)]);  // load column p from A, middle 4 values
        __m256d a_p_l = _mm256_loadu_pd(&a[RAVEL_INDEX(8, p, a_ld)]);  // load column p from A, lower 4 values
        __m256d b_pj; // will contain value Bpj broadcast to vector

        //
        b_pj = _mm256_set1_pd(b[RAVEL_INDEX(p, 0, b_ld)]);
        c_col0_u = _mm256_fmadd_pd(a_p_u, b_pj, c_col0_u);
        c_col0_m = _mm256_fmadd_pd(a_p_m, b_pj, c_col0_m);
        c_col0_l = _mm256_fmadd_pd(a_p_l, b_pj, c_col0_l);
        
        b_pj = _mm256_set1_pd(b[RAVEL_INDEX(p, 1, b_ld)]);
        c_col1_u = _mm256_fmadd_pd(a_p_u, b_pj, c_col1_u);
        c_col1_m = _mm256_fmadd_pd(a_p_m, b_pj, c_col1_m);
        c_col1_l = _mm256_fmadd_pd(a_p_l, b_pj, c_col1_l);

        b_pj = _mm256_set1_pd(b[RAVEL_INDEX(p, 2, b_ld)]);
        c_col2_u = _mm256_fmadd_pd(a_p_u, b_pj, c_col2_u);
        c_col2_m = _mm256_fmadd_pd(a_p_m, b_pj, c_col2_m);
        c_col2_l = _mm256_fmadd_pd(a_p_l, b_pj, c_col2_l);
        
        b_pj = _mm256_set1_pd(b[RAVEL_INDEX(p, 3, b_ld)]);
        c_col3_u = _mm256_fmadd_pd(a_p_u, b_pj, c_col3_u);
        c_col3_m = _mm256_fmadd_pd(a_p_m, b_pj, c_col3_m);
        c_col3_l = _mm256_fmadd_pd(a_p_l, b_pj, c_col3_l);
    }

    // store 12x4 tile back to C
    _mm256_storeu_pd(&c[RAVEL_INDEX(0, 0, c_ld)], c_col0_u);
    _mm256_storeu_pd(&c[RAVEL_INDEX(0, 1, c_ld)], c_col1_u);
    _mm256_storeu_pd(&c[RAVEL_INDEX(0, 2, c_ld)], c_col2_u);
    _mm256_storeu_pd(&c[RAVEL_INDEX(0, 3, c_ld)], c_col3_u);
    _mm256_storeu_pd(&c[RAVEL_INDEX(4, 0, c_ld)], c_col0_m);
    _mm256_storeu_pd(&c[RAVEL_INDEX(4, 1, c_ld)], c_col1_m);
    _mm256_storeu_pd(&c[RAVEL_INDEX(4, 2, c_ld)], c_col2_m);
    _mm256_storeu_pd(&c[RAVEL_INDEX(4, 3, c_ld)], c_col3_m);
    _mm256_storeu_pd(&c[RAVEL_INDEX(8, 0, c_ld)], c_col0_l);
    _mm256_storeu_pd(&c[RAVEL_INDEX(8, 1, c_ld)], c_col1_l);
    _mm256_storeu_pd(&c[RAVEL_INDEX(8, 2, c_ld)], c_col2_l);
    _mm256_storeu_pd(&c[RAVEL_INDEX(8, 3, c_ld)], c_col3_l);
}

void kernel_ji_mbx4(FnKernel fn_kernel, size_t mb, size_t m, size_t n, size_t k, const double* const __restrict__ a, size_t a_ld, const double* const __restrict__ b, size_t b_ld, double* const __restrict__ c, size_t c_ld)
{
    constexpr size_t nb = 4;

    for (size_t j = 0; j < n; j += nb)
    {
        for (size_t i = 0; i < m; i += mb)
        {
            fn_kernel(k, &a[RAVEL_INDEX(i, 0, a_ld)], a_ld, &b[RAVEL_INDEX(0, j, b_ld)], b_ld, &c[RAVEL_INDEX(i, j, c_ld)], c_ld);
        }
    }
}

void kernel_packed_12x4(size_t k, const double* const __restrict__ a, const double* const __restrict__ b, double* const __restrict__ c, size_t c_ld)
{
    constexpr size_t mb = 12;
    constexpr size_t nb = 4;

    // utilise all 16 vector registers - 12 to store C, 3 for A panel, 1 for B panel
    // load 12x4 C tile into vector registers, each column as 4 upper values, 4 middle values, and 4 lower values
    __m256d c_col0_u = _mm256_loadu_pd(&c[RAVEL_INDEX(0, 0, c_ld)]);
    __m256d c_col1_u = _mm256_loadu_pd(&c[RAVEL_INDEX(0, 1, c_ld)]);
    __m256d c_col2_u = _mm256_loadu_pd(&c[RAVEL_INDEX(0, 2, c_ld)]);
    __m256d c_col3_u = _mm256_loadu_pd(&c[RAVEL_INDEX(0, 3, c_ld)]);
    __m256d c_col0_m = _mm256_loadu_pd(&c[RAVEL_INDEX(4, 0, c_ld)]);
    __m256d c_col1_m = _mm256_loadu_pd(&c[RAVEL_INDEX(4, 1, c_ld)]);
    __m256d c_col2_m = _mm256_loadu_pd(&c[RAVEL_INDEX(4, 2, c_ld)]);
    __m256d c_col3_m = _mm256_loadu_pd(&c[RAVEL_INDEX(4, 3, c_ld)]);
    __m256d c_col0_l = _mm256_loadu_pd(&c[RAVEL_INDEX(8, 0, c_ld)]);
    __m256d c_col1_l = _mm256_loadu_pd(&c[RAVEL_INDEX(8, 1, c_ld)]);
    __m256d c_col2_l = _mm256_loadu_pd(&c[RAVEL_INDEX(8, 2, c_ld)]);
    __m256d c_col3_l = _mm256_loadu_pd(&c[RAVEL_INDEX(8, 3, c_ld)]);

    // for each column/row vector pair in A and B
    for (size_t p = 0; p < k; p++)
    {
        //
        __m256d a_p_u = _mm256_load_pd(&a[(p * mb) + 0]);  // load column p from A, upper 4 values
        __m256d a_p_m = _mm256_load_pd(&a[(p * mb) + 4]);  // load column p from A, middle 4 values
        __m256d a_p_l = _mm256_load_pd(&a[(p * mb) + 8]);  // load column p from A, lower 4 values
        __m256d b_pj; // will contain value Bpj broadcast to vector

        //
        b_pj = _mm256_set1_pd(b[(p * nb) + 0]);
        c_col0_u = _mm256_fmadd_pd(a_p_u, b_pj, c_col0_u);
        c_col0_m = _mm256_fmadd_pd(a_p_m, b_pj, c_col0_m);
        c_col0_l = _mm256_fmadd_pd(a_p_l, b_pj, c_col0_l);
        
        b_pj = _mm256_set1_pd(b[(p * nb) + 1]);
        c_col1_u = _mm256_fmadd_pd(a_p_u, b_pj, c_col1_u);
        c_col1_m = _mm256_fmadd_pd(a_p_m, b_pj, c_col1_m);
        c_col1_l = _mm256_fmadd_pd(a_p_l, b_pj, c_col1_l);

        b_pj = _mm256_set1_pd(b[(p * nb) + 2]);
        c_col2_u = _mm256_fmadd_pd(a_p_u, b_pj, c_col2_u);
        c_col2_m = _mm256_fmadd_pd(a_p_m, b_pj, c_col2_m);
        c_col2_l = _mm256_fmadd_pd(a_p_l, b_pj, c_col2_l);
                
        b_pj = _mm256_set1_pd(b[(p * nb) + 3]);
        c_col3_u = _mm256_fmadd_pd(a_p_u, b_pj, c_col3_u);
        c_col3_m = _mm256_fmadd_pd(a_p_m, b_pj, c_col3_m);
        c_col3_l = _mm256_fmadd_pd(a_p_l, b_pj, c_col3_l);
    }

    // store 12x4 tile back to C
    _mm256_storeu_pd(&c[RAVEL_INDEX(0, 0, c_ld)], c_col0_u);
    _mm256_storeu_pd(&c[RAVEL_INDEX(0, 1, c_ld)], c_col1_u);
    _mm256_storeu_pd(&c[RAVEL_INDEX(0, 2, c_ld)], c_col2_u);
    _mm256_storeu_pd(&c[RAVEL_INDEX(0, 3, c_ld)], c_col3_u);
    _mm256_storeu_pd(&c[RAVEL_INDEX(4, 0, c_ld)], c_col0_m);
    _mm256_storeu_pd(&c[RAVEL_INDEX(4, 1, c_ld)], c_col1_m);
    _mm256_storeu_pd(&c[RAVEL_INDEX(4, 2, c_ld)], c_col2_m);
    _mm256_storeu_pd(&c[RAVEL_INDEX(4, 3, c_ld)], c_col3_m);
    _mm256_storeu_pd(&c[RAVEL_INDEX(8, 0, c_ld)], c_col0_l);
    _mm256_storeu_pd(&c[RAVEL_INDEX(8, 1, c_ld)], c_col1_l);
    _mm256_storeu_pd(&c[RAVEL_INDEX(8, 2, c_ld)], c_col2_l);
    _mm256_storeu_pd(&c[RAVEL_INDEX(8, 3, c_ld)], c_col3_l);
}

void pack_row_panel(size_t m, size_t n, size_t nb, const double* const __restrict__ src, size_t src_ld, double* const __restrict__ dst)
{
    // Iterate by column micro-panel, filling each
    for (size_t j = 0; j < n; j += nb)
    {
        const size_t jb = std::min(nb, n - j);
        pack_column_micropanel(m, jb, &src[RAVEL_INDEX(0, j, src_ld)], src_ld, &dst[m * j]);
    }
}

void pack_column_micropanel(size_t m, size_t n, const double* const __restrict__ src, size_t src_ld, double* __restrict__ dst)
{
    // Iterate by row then column (SRCij) to fill dst rows contiguously
    for (size_t i = 0; i < m; i++)
    {
        for (size_t j = 0; j < n; j++)
        {
            *(dst++) = src[RAVEL_INDEX(i, j, src_ld)];
        }
    }
}

void pack_tile(size_t m, size_t n, size_t mb, const double* const __restrict__ src, size_t src_ld, double* const __restrict__ dst)
{
    // Iterate by row micro-panel, filling each
    for (size_t i = 0; i < m; i += mb)
    {
        const size_t ib = std::min(mb, m - i);
        pack_row_micropanel(ib, n, &src[RAVEL_INDEX(i, 0, src_ld)], src_ld, &dst[n * i]);
    }
}

void pack_row_micropanel(size_t m, size_t n, const double* const __restrict__ src, size_t src_ld, double* __restrict__ dst)
{
    // Iterate by column then row (SRCij) to fill dst columns contiguously
    for (size_t j = 0; j < n; j++)
    {
        for (size_t i = 0; i < m; i++)
        {
            *(dst)++ = src[RAVEL_INDEX(i, j, src_ld)];
        }
    }
}


}

#undef ASSERT_MATRIX_DIMS_COMPATIBLE
