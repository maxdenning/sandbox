#include "hashmap/aligned_oasimd_hashmap.hpp"

#include <iostream>
#include <bitset>
#include <array>
#include <cstring>
#include "ostream_helpers.hpp"
#include "simd_wrapper.hpp"

#include <mm_malloc.h>

namespace sandbox
{


//
template class AlignedOASIMDHashMap<uint64_t, uint64_t>;
template class AlignedOASIMDHashMap<uint64_t, std::string>;
template class AlignedOASIMDHashMap<uint32_t, std::string>;
template class AlignedOASIMDHashMap<uint16_t, std::string>;


template<typename K, typename M>
AlignedOASIMDHashMap<K, M>::AlignedOASIMDHashMap(size_t table_size)
    : hasher(std::hash<K>{}),
      m_table_size((table_size + (table_size % W::count)) / W::count),  //((table_size + (table_size % PACKED_VALUES)) / PACKED_VALUES),
      m_key_blocks(new (std::align_val_t(AVX2_ALIGN)) KeyBlock[m_table_size]),
      m_values(new std::unique_ptr<M>[m_table_size * W::count]()) // PACKED_VALUES]())
{

    for (size_t b = 0; b < m_table_size; b++)
    {
        //std::memset(&m_key_blocks[b], 0xFF, sizeof(KeyBlock));

        //for (size_t k = 0; k < PACKED_VALUES; k++)
        for (size_t k = 0; k < W::count; k++)
        {
            m_key_blocks[b].keys[k] = IS_AVAILABLE;
        }
    }    
}

template<typename K, typename M>
AlignedOASIMDHashMap<K, M>::~AlignedOASIMDHashMap()
{
    // delete bucket table
    delete[] m_key_blocks;
    delete[] m_values;
}

template<typename K, typename M>
void AlignedOASIMDHashMap<K, M>::insert(HashMapEntry<K, M>&& entry)
{
/*
    const __m256i is_available_vec = _mm256_set1_epi64x(IS_AVAILABLE);

    const size_t offset = hasher(entry.key) % m_table_size; //bucket_index(entry.key);
    for (size_t t = 0; t < m_table_size; t++)
    {
        const size_t index = (offset + t) % m_table_size;  // wrap around index

        const __m256i keys_vec = _mm256_load_si256((__m256i*)(m_key_blocks + index));  // using aligned version would be faster
        const __m256i res = _mm256_cmpeq_epi64(keys_vec, is_available_vec);
        const uint32_t res_mask = static_cast<uint32_t>(_mm256_movemask_epi8(res));

        //std::cout << index << "/" << m_table_size << ": " << to_array<uint64_t>(keys_vec) << ", " << std::bitset<32>(res_mask) << std::endl;
        
        // << ", " << std::bitset<32>(res_mask) << ", " << (res_mask > 0) << ", " << __builtin_ffs(res_mask) << " " << (__builtin_ffs(res_mask) - 1U) / 8U << std::endl;

        /*
        res will have some configuration of high and low blocks, from which the least significant high block needs to be identified.
            [0000 0000]x8 [0000 0000]x8 [0000 0000]x8 [1111 1111]x8  -> 4xint64_t (movemask_epi8) 1xint32_t -> 0000 0000 0000 0000 0000 0000 1111 1111

             0000 0000 0000 0000 0000 0000 1111 1111 -> __builtin_ffs ->  1 -> -1, /8 -> 0
             0000 0000 0000 0000 1111 1111 0000 0000 -> __builtin_ffs ->  9 -> -1, /8 -> 1
             0000 0000 1111 1111 0000 0000 0000 0000 -> __builtin_ffs -> 17 -> -1, /8 -> 2
             1111 1111 0000 0000 0000 0000 0000 0000 -> __builtin_ffs -> 25 -> -1, /8 -> 3
        *//*
        if (res_mask > 0)
        {
            const uint32_t block_index = (__builtin_ffs(res_mask) - 1U) / 8U;
            m_key_blocks[index].keys[block_index] = entry.key;
            m_values[(index * PACKED_VALUES) + block_index] = std::make_unique<M>(entry.value);

            //std::cout << "__ " << index << "->" << block_index << ", " << (index * PACKED_VALUES) + block_index << " __" << std::endl;
            return;
        }
    }
*/

    const vector_type is_available_vec = W::full(IS_AVAILABLE);

    const size_t offset = hasher(entry.key) % m_table_size;
    for (size_t t = 0; t < m_table_size; t++)
    {
        const size_t index = (offset + t) % m_table_size;  // wrap around index

        const vector_type key_block_vec = W::load((K*)(m_key_blocks + index));
        const vector_type comparison = W::cmp_eq(key_block_vec, is_available_vec);
        const uint32_t comparison_mask = static_cast<uint32_t>(_mm256_movemask_epi8(comparison));

        /*
        "comparison" will have W::count blocks, mixed high and low. The least significant high block needs to be identified.
            e.g.
            [0000 0000]x8 [0000 0000]x8 [0000 0000]x8 [1111 1111]x8  -> 4xint64_t (movemask_epi8) 1xint32_t -> 0000 0000 0000 0000 0000 0000 1111 1111

            [[0000 0000]x8]x 3 [[1111 1111]x8]x1 ->  4xuint64_t (movemask_epi8) 1xint32_t -> 0000 0000 0000 0000 0000 0000 1111 1111
            [[0000 0000]x4]x 7 [[1111 1111]x4]x1 ->  8xuint32_t (movemask_epi8) 1xint32_t -> 0000 0000 0000 0000 0000 0000 0000 1111
            [[0000 0000]x1]x15 [[1111 1111]x1]x1 -> 16xuint16_t (movemask_epi8) 1xint32_t -> 0000 0000 0000 0000 0000 0000 0000 0011

             0000 0000 0000 0000 0000 0000 1111 1111 -> __builtin_ffs ->  1 -> -1, /8 -> 0
             0000 0000 0000 0000 1111 1111 0000 0000 -> __builtin_ffs ->  9 -> -1, /8 -> 1
             0000 0000 1111 1111 0000 0000 0000 0000 -> __builtin_ffs -> 17 -> -1, /8 -> 2
             1111 1111 0000 0000 0000 0000 0000 0000 -> __builtin_ffs -> 25 -> -1, /8 -> 3
        */

        if (comparison_mask > 0)
        {
            const uint32_t block_index = (__builtin_ffs(comparison_mask) - 1U) / (32U / W::count);
            m_key_blocks[index].keys[block_index] = entry.key;
            m_values[(index * W::count) + block_index] = std::make_unique<M>(entry.value);
            return;
        }
    }

    throw std::out_of_range("free cell not found");
}

template<typename K, typename M>
M AlignedOASIMDHashMap<K, M>::erase(const K key)
{
    return (M)0; // TODO: temporary
}

template<typename K, typename M>
M& AlignedOASIMDHashMap<K, M>::at(const K key)
{
    if (key == IS_AVAILABLE)
    {
        throw std::invalid_argument("reserved key value");
    }
/*
    const __m256i query_key_vec = _mm256_set1_epi64x(key);

    const size_t offset = hasher(key) % m_table_size; //bucket_index(key);
    for (size_t b = 0; b < m_table_size; b++)
    {
        const size_t index = (offset + b) % m_table_size;  // wrap around index

        const __m256i keys_vec = _mm256_load_si256((__m256i*)(m_key_blocks + index));
        const __m256i res = _mm256_cmpeq_epi64(keys_vec, query_key_vec);  // this will have at most one high block, which needs to be identified
        const uint32_t res_mask = static_cast<uint32_t>(_mm256_movemask_epi8(res));

        if (res_mask > 0)
        {
            const uint32_t block_index = (__builtin_ffs(res_mask) - 1U) / 8U; 
            return *m_values[(index * PACKED_VALUES) + block_index].get();
        }
    }
*/

    const vector_type query_key_vec = W::full(key);

    const size_t offset = hasher(key) % m_table_size;
    for (size_t b = 0; b < m_table_size; b++)
    {
        const size_t index = (offset + b) % m_table_size;  // wrap around index

        const vector_type key_block_vec = W::load((K*)(m_key_blocks + index));
        const vector_type comparison = W::cmp_eq(key_block_vec, query_key_vec);  // this will have at most one high block
        const uint32_t comparison_mask = static_cast<uint32_t>(_mm256_movemask_epi8(comparison));

        if (comparison_mask > 0)
        {
            const uint32_t block_index = (__builtin_ffs(comparison_mask) - 1U) / (32U / W::count);
            return *m_values[(index * W::count) + block_index].get();
        }
    }

    throw std::out_of_range("key not found");
}

template<typename K, typename M>
size_t AlignedOASIMDHashMap<K, M>::bucket_count() const
{
    return m_table_size;
}

template<typename K, typename M>
uint32_t AlignedOASIMDHashMap<K, M>::collision_count() const
{
    return 0;
}

template<typename K, typename M>
size_t AlignedOASIMDHashMap<K, M>::bucket_depth(size_t index) const
{
    return W::count; //PACKED_VALUES;
}


}
