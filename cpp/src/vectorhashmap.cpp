#include "hashmap/vectorhashmap.hpp"

namespace sandbox
{


//
template class VectorHashMap<std::string, std::string>;
template class VectorHashMap<uint64_t, uint64_t>;
template class VectorHashMap<uint32_t, uint32_t>;
template class VectorHashMap<uint16_t, uint16_t>;
template class VectorHashMap<uint8_t, uint8_t>;

template<typename K, typename M>
VectorHashMap<K, M>::VectorHashMap(size_t table_size)
    : m_table_size(table_size)
{
    // allocate bucket table
    m_table = new std::vector<HashMapEntry<K, M>>[m_table_size]();
}

template<typename K, typename M>
VectorHashMap<K, M>::~VectorHashMap()
{
    // delete bucket table
    delete[] m_table;
}

template<typename K, typename M>
void VectorHashMap<K, M>::insert(HashMapEntry<K, M>&& entry)
{
    const size_t idx = this->bucket_index(entry.key, m_table_size);

    // record key collision
    if (!m_table[idx].empty())
    {
        this->m_collisions++;
    }

    // search for existing matching entry
    for (auto& e : m_table[idx])
    {
        if (e.key == entry.key)
        {
            // modify existing value
            e.value = entry.value;
            return;
        }
    }

    // create new value
    m_table[idx].push_back(std::move(entry));
}

template<typename K, typename M>
M VectorHashMap<K, M>::erase(const K& key)
{
    std::vector<HashMapEntry<K, M>>& bucket = m_table[this->bucket_index(key, m_table_size)];
    for (auto itr = bucket.begin(); itr != bucket.end(); itr++)
    {
        if (itr->key == key)
        {
            M value = itr->value;
            bucket.erase(itr);
            return value;
        }
    }
    throw std::out_of_range("key not found");
}

template<typename K, typename M>
M& VectorHashMap<K, M>::at(const K& key)
{
    const size_t idx = this->bucket_index(key, m_table_size);
    for (auto& entry : m_table[idx])
    {
        if (entry.key == key)
        {
            return entry.value;
        }
    }
    throw std::out_of_range("key not found");
}

template<typename K, typename M>
size_t VectorHashMap<K, M>::bucket_count() const
{
    return m_table_size;
}

template<typename K, typename M>
size_t VectorHashMap<K, M>::bucket_depth(size_t index) const
{
    return m_table[index].size();
}


}
