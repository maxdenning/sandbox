#include "hashmap/oasimdhashmap.hpp"

#include <iostream>
#include <bitset>
#include <array>
#include <cstring>
#include "ostream_helpers.hpp"

#include <mm_malloc.h>

namespace sandbox
{


//
template class OASIMDHashMap<uint64_t, uint64_t>;
template class OASIMDHashMap<uint64_t, std::string>;
template class OASIMDHashMap<uint32_t, std::string>;
template class OASIMDHashMap<uint16_t, std::string>;


template<typename K, typename M>
OASIMDHashMap<K, M>::OASIMDHashMap(size_t table_size)
    : hasher(std::hash<K>{}),
      m_max_table_index(table_size),
      m_table_size(table_size + (W::count - 1U)),
      m_keys(new(std::align_val_t(32)) K[m_table_size]),
      m_values(new std::unique_ptr<M>[m_table_size]())
{
    for (size_t i = 0; i < m_table_size; i++)
    {
        m_keys[i] = IS_AVAILABLE;
    }
}

template<typename K, typename M>
OASIMDHashMap<K, M>::~OASIMDHashMap()
{
    // delete bucket table
    delete[] m_keys;
    delete[] m_values;
}

template<typename K, typename M>
void OASIMDHashMap<K, M>::insert(HashMapEntry<K, M>&& entry)
{
    if (entry.key == IS_AVAILABLE)
    {
        throw std::invalid_argument("reserved key value");
    }

/*
    const __m256i is_available_vec = _mm256_set1_epi64x(IS_AVAILABLE);

    const size_t offset = hasher(entry.key) % m_max_table_index;
    for (size_t t = 0; t < m_max_table_index; t += W::count)
    {
        const size_t index = (offset + t) % m_max_table_index;  // wrap around index

        const __m256i keys_vec = _mm256_loadu_si256((__m256i*)(m_keys + index));  // using aligned version would be faster
        const __m256i res = _mm256_cmpeq_epi64(keys_vec, is_available_vec);
        const uint32_t res_mask = static_cast<uint32_t>(_mm256_movemask_epi8(res));

        //alignas(16) std::array<uint64_t, 4> view;
        //_mm256_storeu_si256((__m256i*)view.data(), res);
        //std::cout << index << "/" << m_table_size << ": " << view << ", " << std::bitset<32>(res_mask) << ", " << (res_mask > 0) << ", " << __builtin_ffs(res_mask) << " " << (__builtin_ffs(res_mask) - 1U) / 8U << std::endl;

        /*
        res will have some configuration of high and low blocks, from which the least significant high block needs to be identified.
            [0000 0000]x8 [0000 0000]x8 [0000 0000]x8 [1111 1111]x8  -> 4xint64_t (movemask_epi8) 1xint32_t -> 0000 0000 0000 0000 0000 0000 1111 1111

             0000 0000 0000 0000 0000 0000 1111 1111 -> __builtin_ffs ->  1 -> -1, /8 -> 0
             0000 0000 0000 0000 1111 1111 0000 0000 -> __builtin_ffs ->  9 -> -1, /8 -> 1
             0000 0000 1111 1111 0000 0000 0000 0000 -> __builtin_ffs -> 17 -> -1, /8 -> 2
             1111 1111 0000 0000 0000 0000 0000 0000 -> __builtin_ffs -> 25 -> -1, /8 -> 3
        *//*
        if (res_mask > 0)
        {
            const uint32_t key_offset = (__builtin_ffs(res_mask) - 1U) / 8U;            
            m_keys[index + key_offset] = entry.key;
            m_values[index + key_offset] = std::make_unique<M>(entry.value);
            return;
        }
    }
*/
/*
    const vector_type is_available_vec = W::full(IS_AVAILABLE);
    const size_t offset = hasher(entry.key) % m_max_table_index;
    for (size_t t = 0; t < m_max_table_index; t += W::count)
    {
        const size_t index = (offset + t) % m_max_table_index;  // wrap around index

        const vector_type key_vec = W::loadu((K*)(m_keys + index));
        const vector_type comparison = W::cmp_eq(key_vec, is_available_vec);
        const uint32_t comparison_mask = static_cast<uint32_t>(_mm256_movemask_epi8(comparison));

        /*
        "comparison" will have W::count blocks, mixed high and low. The least significant high block needs to be identified.
            e.g.
            [0000 0000]x8 [0000 0000]x8 [0000 0000]x8 [1111 1111]x8  -> 4xint64_t (movemask_epi8) 1xint32_t -> 0000 0000 0000 0000 0000 0000 1111 1111

            [[0000 0000]x8]x 3 [[1111 1111]x8]x1 ->  4xuint64_t (movemask_epi8) 1xint32_t -> 0000 0000 0000 0000 0000 0000 1111 1111
            [[0000 0000]x4]x 7 [[1111 1111]x4]x1 ->  8xuint32_t (movemask_epi8) 1xint32_t -> 0000 0000 0000 0000 0000 0000 0000 1111
            [[0000 0000]x1]x15 [[1111 1111]x1]x1 -> 16xuint16_t (movemask_epi8) 1xint32_t -> 0000 0000 0000 0000 0000 0000 0000 0011

             0000 0000 0000 0000 0000 0000 1111 1111 -> __builtin_ffs ->  1 -> -1, /8 -> 0
             0000 0000 0000 0000 1111 1111 0000 0000 -> __builtin_ffs ->  9 -> -1, /8 -> 1
             0000 0000 1111 1111 0000 0000 0000 0000 -> __builtin_ffs -> 17 -> -1, /8 -> 2
             1111 1111 0000 0000 0000 0000 0000 0000 -> __builtin_ffs -> 25 -> -1, /8 -> 3
        *//*

        if (comparison_mask > 0)
        {
            const uint32_t key_offset = (__builtin_ffs(comparison_mask) - 1U) / (32U / W::count);
            m_keys[index + key_offset] = entry.key;
            m_values[index + key_offset] = std::make_unique<M>(entry.value);
            return;
        }
    }
*/

    const size_t index = find_first(hasher(entry.key) % m_max_table_index, IS_AVAILABLE);
    if (index != std::numeric_limits<size_t>::max())
    {
        m_keys[index] = entry.key;
        m_values[index] = std::make_unique<M>(entry.value);
    }

    throw std::out_of_range("free cell not found");
}

template<typename K, typename M>
M OASIMDHashMap<K, M>::erase(const K key)
{
    return (M)0; // TODO: temporary
}

template<typename K, typename M>
M& OASIMDHashMap<K, M>::at(const K key)
{
    if (key == IS_AVAILABLE)
    {
        throw std::invalid_argument("reserved key value");
    }
/*
    const __m256i query_key_vec = _mm256_set1_epi64x(key);

    const size_t offset = hasher(key) % m_max_table_index;
    for (size_t t = 0; t < m_max_table_index; t += W::count)
    {
        const size_t index = (offset + t) % m_max_table_index;  // wrap around index

        const __m256i keys_vec = _mm256_loadu_si256((__m256i*)(m_keys + index));  // using aligned version would be faster
        const __m256i res = _mm256_cmpeq_epi64(keys_vec, query_key_vec);  // res will have at most one high block
        const uint32_t res_mask = static_cast<uint32_t>(_mm256_movemask_epi8(res));

        if (res_mask > 0)
        {
            const uint32_t key_offset = (__builtin_ffs(res_mask) - 1U) / 8U; 
            return *m_values[index + key_offset].get();
        }
    }
*/
/*
    const vector_type query_key_vec = W::full(key);
    const size_t offset = hasher(key) % m_max_table_index;
    for (size_t t = 0; t < m_max_table_index; t += W::count)
    {
        const size_t index = (offset + t) % m_max_table_index;  // wrap around index
    
        const vector_type key_vec = W::loadu((K*)(m_keys + index));
        const vector_type comparison = W::cmp_eq(key_vec, query_key_vec);  // this will have at most one high block
        const uint32_t comparison_mask = static_cast<uint32_t>(_mm256_movemask_epi8(comparison));
    
        if (comparison_mask > 0)
        {
            const uint32_t key_offset = (__builtin_ffs(comparison_mask) - 1U) / (32U / W::count);
            return *m_values[index + key_offset].get();
        }
    }
*/

    const size_t index = find_first(hasher(key) % m_max_table_index, key);
    if (index != std::numeric_limits<size_t>::max())
    {
        return *m_values[index].get();
    }

    throw std::out_of_range("key not found");
}

template<typename K, typename M>
size_t OASIMDHashMap<K, M>::bucket_count() const
{
    return m_max_table_index;
}

template<typename K, typename M>
uint32_t OASIMDHashMap<K, M>::collision_count() const
{
    return 0;
}

template<typename K, typename M>
size_t OASIMDHashMap<K, M>::bucket_depth(size_t index) const
{
    return 1;
}

template<typename K, typename M>
size_t OASIMDHashMap<K, M>::find_first(const size_t start, const K value) const
{
    const vector_type query_vec = W::full(value);

    for (size_t t = 0U; t < m_max_table_index; t += W::count)
    {
        const size_t index = (start + t) % m_max_table_index;  // wrap around index

        const vector_type key_vec = W::loadu((K*)(m_keys + index));
        const vector_type comparison = W::cmp_eq(key_vec, query_vec);
        const uint32_t comparison_mask = static_cast<uint32_t>(_mm256_movemask_epi8(comparison));

        if (comparison_mask > 0)
        {
            return index + (__builtin_ffs(comparison_mask) - 1U) / (32U / W::count);
        }
    }

    return std::numeric_limits<size_t>::max();
}


}
