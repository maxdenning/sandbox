#include "hashmap/cacheawarehashmap.hpp"

#include <iostream>

namespace sandbox
{


//
template class CacheAwareHashMap<std::string, std::string>;
template class CacheAwareHashMap<uint64_t, uint64_t>;
template class CacheAwareHashMap<uint32_t, uint32_t>;
template class CacheAwareHashMap<uint16_t, uint16_t>;
template class CacheAwareHashMap<uint8_t, uint8_t>;


template<typename K, typename M>
CacheAwareHashMap<K, M>::CacheAwareHashMap(size_t table_size)
    : m_table_size(table_size)
{
    // allocate bucket table
    m_table = new std::unique_ptr<Node>[m_table_size]();

//    std::cout << sizeof(Node) << " = ("
//              << sizeof(Node::keys) << " " << sizeof(Node::values) << " " << sizeof(Node::next) << " " << sizeof(Node::back) << "), "
//              << Node::NODE_SIZE << " " << Node::N_PREFETCHED_NODES << std::endl; 
}

template<typename K, typename M>
CacheAwareHashMap<K, M>::~CacheAwareHashMap()
{
    // delete bucket table
    delete[] m_table;
}

template<typename K, typename M>
void CacheAwareHashMap<K, M>::insert(HashMapEntry<K, M>&& entry)
{
    const size_t idx = this->bucket_index(entry.key, m_table_size);
    Node* node = m_table[idx].get();

    // create new linked list in table
    if (node == nullptr)
    {
        m_table[idx] = std::make_unique<Node>(std::move(entry));
        return;
    }

    // record key collision
    this->m_collisions++;

    // search linked list for existing matching entry or nodes which aren't full
    while (node != nullptr)
    {
        // search for matching entry to modify (free prefetched lookup)
        for (size_t i = 0; i < node->back; i++)
        {
            if (node->keys[i] == entry.key)
            {
                node->values[i] = entry.value;
                return;
            }
        }

        if (!node->is_full())
        {
            // push into current node if there is space
            node->push_back(std::move(entry));
            return;
        }
        
        if (node->next == nullptr)
        {
            // create next node
            node->next = new Node(std::move(entry));  //std::make_unique<Node>(std::move(entry));
            return;
        }

        // move on to next node
        node = node->next;//.get();
    }

/*
    const size_t idx = this->bucket_index(entry.key, m_table_size);
    Node* node = m_table[idx].get();

    if (node == nullptr)
    {
        // create new linked list in table
        m_table[idx] = std::make_unique<Node>(std::move(entry));
    }
    else
    {
        // record key collision
        this->m_collisions++;

        // insert into node
        node->insert(std::move(entry));
    }
*/



/*
    const size_t idx = this->bucket_index(entry.key, m_table_size);
    Node* parent = nullptr;
    Node* node = m_table[idx].get();

    // create new linked list in table
    if (node == nullptr)
    {
        m_table[idx] = std::make_unique<Node>();
        m_table[idx]->push_back(std::move(entry));
        return;
    }

    // record key collision
    this->m_collisions++;

    // search linked list for existing matching entry or nodes which aren't full
    while (node != nullptr)
    {
        // search for matching entry to modify (free prefetched lookup)
        for (auto& e : node->entries)
        {
            if (e.key == entry.key)
            {
                e.value = entry.value;
                return;
            }
        }

        if (node->is_full())
        {
            // move on to next node
            parent = node;
            node = node->next.get();
        }
        else
        {
            // add entry to node and exit
            node->push_back(std::move(entry));
            return;
        }
    }

    // create new node in linked list
    parent->next = std::make_unique<Node>();
    parent->next->push_back(std::move(entry));
*/
}

template<typename K, typename M>
M CacheAwareHashMap<K, M>::erase(const K& key)
{
    return (M)0; // TODO: temporary
}

template<typename K, typename M>
M& CacheAwareHashMap<K, M>::at(const K& key)
{
    Node* node = m_table[this->bucket_index(key, m_table_size)].get();

    while (node != nullptr)
    {
        // search node for matching key
        /*for (auto& entry : node->entries)
        {
            if (entry.key == key)
            {
                return entry.value;
            }
        }*/

        for (uint8_t i = 0; i < node->back; i++)
        {
            if (node->keys[i] == key)
            {
                return node->values[i];
            }
        }

        // move on to next node
        node = node->next;//.get();
    }

    throw std::out_of_range("key not found");
}

template<typename K, typename M>
size_t CacheAwareHashMap<K, M>::bucket_count() const
{
    return m_table_size;
}

template<typename K, typename M>
size_t CacheAwareHashMap<K, M>::bucket_depth(size_t index) const
{
    if (m_table[index] == nullptr)
    {
        return 0;
    }
    return m_table[index]->size();
}

template<typename K, typename M>
CacheAwareHashMap<K, M>::Node::Node()
    //: back(entries.begin()), next(nullptr)
    : next(nullptr), back(0)
{
}

template<typename K, typename M>
CacheAwareHashMap<K, M>::Node::Node(HashMapEntry<K, M>&& entry)
    //: back(entries.begin()), next(nullptr)
    : next(nullptr), back(1)
{
    //*back = entry;
    //back++;

    keys[0] = entry.key;
    values[0] = entry.value;
}

template<typename K, typename M>
CacheAwareHashMap<K, M>::Node::~Node()
{
    if (next != nullptr)
    {
        delete next;
    }
}

template<typename K, typename M>
inline void CacheAwareHashMap<K, M>::Node::push_back(HashMapEntry<K, M>&& entry)
{
    //*back = entry;
    //back++;

    keys[back] = entry.key;
    values[back] = entry.value;
    back++;
}

/*template<typename K, typename M>
void CacheAwareHashMap<K, M>::Node::insert(HashMapEntry<K, M>&& entry)
{
    // search for matching entry to modify (free prefetched lookup)
    for (auto& existing : entries)
    {
        if (existing.key == entry.key)
        {
            existing.value = entry.value;
            return;
        }
    }

    if (is_full())
    {
        if (next == nullptr)
        {
            // create next node
            next = std::make_unique<Node>(std::move(entry));
        }
        else
        {
            // insert entry into next node
            next->insert(std::move(entry));
        }
    }
    else
    {
        // push into current node if there is space
        *back = entry;
        back++;
    }
*/
/*
    if (is_full())
    {
        // create next node if necessary
        if (next == nullptr)
        {
            next = std::make_unique<Node>();
        }

        // insert entry into next node
        next->insert(std::move(entry));
    }
    else
    {
        // push back of current node
        *back = entry;
        back++;
    }
*/
//}

template<typename K, typename M>
inline bool CacheAwareHashMap<K, M>::Node::is_full() const
{
    //return back == entries.end();
    return back >= N_PREFETCHED_NODES;
}

template<typename K, typename M>
size_t CacheAwareHashMap<K, M>::Node::size() const
{
    //const auto const_back = typename decltype(entries)::const_iterator(back);
    //return std::distance(entries.begin(), const_back) + (next == nullptr ? 0 : next->size());

    return back + (next == nullptr ? 0 : next->size());
}

}
