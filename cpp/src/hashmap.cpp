#include "hashmap/hashmap.hpp"

namespace sandbox
{


//
template class HashMap<std::string, std::string>;
template class HashMap<int64_t, int64_t>;
template class HashMap<uint64_t, uint64_t>;
template class HashMap<uint32_t, uint32_t>;
template class HashMap<uint16_t, uint16_t>;
template class HashMap<uint8_t, uint8_t>;

template<typename K, typename M>
HashMap<K, M>::HashMap()
    : m_collisions(0), hasher(std::hash<K>{})
{
}

template<typename K, typename M>
uint32_t HashMap<K, M>::report_collisions() const
{
    return m_collisions;
}

template<typename K, typename M>
std::vector<size_t> HashMap<K, M>::report_bucket_depths() const
{
    std::vector<size_t> depths(bucket_count(), 0);
    for (size_t i = 0; i < bucket_count(); i++)
    {
        depths[i] = bucket_depth(i);
    }
    return depths;
}

template<typename K, typename M>
size_t HashMap<K, M>::bucket_index(const K& key, size_t bucket_count)
{
    return hasher(key) % bucket_count;
}


}
