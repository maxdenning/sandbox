#include "simd_wrapper.hpp"
#include "ostream_helpers.hpp"

namespace sandbox
{


std::ostream& operator<<(std::ostream& os, __m256 v) 
{
    alignas(16) std::array<float, 8> view;
    _mm256_storeu_ps(view.data(), v);
    os << view;
    return os;
}

std::ostream& operator<<(std::ostream& os, __m128 v) 
{
    alignas(16) std::array<float, 4> view;
    _mm_storeu_ps(view.data(), v);
    os << view;
    return os;
}


}
