#include "vec/dot_product.hpp"

namespace sandbox
{


//static inline __m256 mul_8ps(const float* a, const float* b);
//static inline __m256 fma_8ps(const float* a, const float* b, const __m256 acc);

//////////////////////////////////////////////////////////////////////////////////////////////////
// PUBLIC
//////////////////////////////////////////////////////////////////////////////////////////////////

float dot_product_scalar(const float* a, const float* b, size_t size)
{
    float out = 0.0f;
    for (size_t i = 0; i < size; i++)
    {
        out += a[i] * b[i];
    }
    return out;
}
/*
float dot_product_simd_avx256(const float* a, const float* b, size_t size)
{
    if (size == 0)
    {
        return 0.0f;
    }

    if ((size % 32) != 0)
    {
        throw std::invalid_argument("size must be divisible by 32");
    }

    //
    const float* const a_end = a + size;

    // process first 32 (4x8) floats, initialise dot product accumulators
    __m256 acc0 = mul_8ps(a,      b);
    __m256 acc1 = mul_8ps(a +  8, b +  8);
    __m256 acc2 = mul_8ps(a + 16, b + 16);
    __m256 acc3 = mul_8ps(a + 24, b + 24);
    a += (8 * 4);
    b += (8 * 4);

    // process the remaining floats
    while (a < a_end)
    {
        acc0 = fma_8ps(a,      b,      acc0);
        acc1 = fma_8ps(a +  8, b +  8, acc1);
        acc2 = fma_8ps(a + 16, b + 16, acc2);
        acc3 = fma_8ps(a + 24, b + 24, acc3);
        a += (8 * 4);
        b += (8 * 4);
    }

    // add 4x8 float accumulators into 1x8 floats
    const __m256 acc01 = _mm256_add_ps(acc0, acc1);
    const __m256 acc23 = _mm256_add_ps(acc2, acc3);
    const __m256 reduce8 = _mm256_add_ps(acc01, acc23);

    // add upper and lower halves of 1x8 floats into 1x4 floats
    const __m128 upper4 = _mm256_castps256_ps128(reduce8);
    const __m128 lower4 = _mm256_extractf128_ps(reduce8, 1);
    const __m128 reduce4 = _mm_add_ps(upper4, lower4);

    // add upper and lower halves of 1x4 floats into 1x2 floats
    const __m128 upper2 = _mm_movehl_ps(reduce4, reduce4);
    const __m128 reduce2 = _mm_add_ps(reduce4, upper2);

    // add 2 values into 1
    const __m128 dup = _mm_movehdup_ps(reduce2);
    const __m128 reduce1 = _mm_add_ss(reduce2, dup);

    // return lowest lane of result
    return _mm_cvtss_f32(reduce1);
}
*/
//////////////////////////////////////////////////////////////////////////////////////////////////
// PRIVATE
//////////////////////////////////////////////////////////////////////////////////////////////////
/*
inline __m256 mul_8ps(const float* a, const float* b)
{
    const __m256 load_a = _mm256_load_ps(a);
    const __m256 load_b = _mm256_load_ps(b);
    return _mm256_mul_ps(load_a, load_b);
}

inline __m256 fma_8ps(const float* a, const float* b, const __m256 acc)
{
    const __m256 load_a = _mm256_load_ps(a);
    const __m256 load_b = _mm256_load_ps(b);
    return _mm256_fmadd_ps(load_a, load_b, acc);
}
*/

}
